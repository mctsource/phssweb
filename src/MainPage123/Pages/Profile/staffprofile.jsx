/**
 * TermsCondition Page
 */
import React, { Component } from 'react';
import { Helmet } from "react-helmet";
import {Avatar_02,Avatar_05,Avatar_09,Avatar_10,Avatar_16 } from '../../../Entryfile/imagepath'

export default class EmployeeProfile extends Component {
  render() {
    return (
        <div className="page-wrapper">
            <Helmet>
              <title>Employee Profile - PARTICIPATION HOUSE SUPPORT SERVICES</title>
              <meta name="description" content="Reactify Blank Page" />
            </Helmet>

            {/* Page Content */}
            <div className="content container-fluid">
              {/* Page Header */}

              <div className="page-header">
                <div className="row">
                  <div className="col-sm-4">
                    <h5 className="page-title"><small className="text-info">Marshall Dunn</small> ID:0100</h5>
                  </div>
                  <div className="col-sm-4">
                    <h5 className="page-title">Role: <small className="text-info">Staff Method</small></h5>
                  </div>
                  <div className="col-sm-4">
                    <h5 className="page-title">Primary Location: <small className="text-info">Belgrave</small></h5>
                  </div>
                </div>
              </div>

              <div className="page-header">
                <div className="row">
                  <div className="col-sm-12">
                    <h3 className="page-title">Profile</h3>
                    <ul className="breadcrumb">
                      <li className="breadcrumb-item"><a href="/app/main/dashboard">Dashboard</a></li>
                      <li className="breadcrumb-item active">Profile</li>
                    </ul>
                  </div>
                </div>
              </div>
              {/* /Page Header */}
              <div className="card mb-0">
                <div className="card-body">
                  <div className="row">
                    <div className="col-md-12">
                      <div className="profile-view">
                        <div className="profile-img-wrap">
                          <div className="profile-img">
                            <a href="#"><img alt="" src={Avatar_02} /></a>
                          </div>
                        </div>
                        <div className="profile-basic">
                          <div className="row">
                            <div className="col-md-5">
                              <div className="profile-info-left">
                                <h3 className="user-name m-t-0 mb-0">John Doe</h3>
                                <h6 className="text-muted">UI/UX Design Team</h6>
                                <small className="text-muted">Web Designer</small>
                                <div className="staff-id">Employee ID : FT-0001</div>
                                <div className="small doj text-muted">Date of Join : 1st Jan 2013</div>
                                <div className="staff-msg"><a className="btn btn-custom" href="/conversation/chat">Send Message</a></div>
                              </div>
                            </div>
                            <div className="col-md-7">
                              <ul className="personal-info">
                                <li>
                                  <div className="title">Phone:</div>
                                  <div className="text"><a href="">9876543210</a></div>
                                </li>
                                <li>
                                  <div className="title">Email:</div>
                                  <div className="text"><a href="">johndoe@example.com</a></div>
                                </li>
                                <li>
                                  <div className="title">Birthday:</div>
                                  <div className="text">24th July</div>
                                </li>
                                <li>
                                  <div className="title">Address:</div>
                                  <div className="text">1861 Bayonne Ave, Manchester Township, NJ, 08759</div>
                                </li>
                                <li>
                                  <div className="title">Gender:</div>
                                  <div className="text">Male</div>
                                </li>
                                <li>
                                  <div className="title">Reports to:</div>
                                  <div className="text">
                                    <div className="avatar-box">
                                      <div className="avatar avatar-xs">
                                        <img src={Avatar_16} alt="" />
                                      </div>
                                    </div>
                                    <a href="/app/profile/employee-profile">
                                      Jeffery Lalor
                                    </a>
                                  </div>
                                </li>
                              </ul>
                            </div>
                          </div>
                        </div>
                        <div className="pro-edit"><a data-target="#profile_info" data-toggle="modal" className="edit-icon" href="#"><i className="fa fa-pencil" /></a></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="card tab-box">
                <div className="row user-tabs">
                  <div className="col-lg-12 col-md-12 col-sm-12 line-tabs">
                    <ul className="nav nav-tabs nav-tabs-bottom">
                      <li className="nav-item"><a href="#profile_tab" data-toggle="tab" className="nav-link active">Profile</a></li>
                      <li className="nav-item"><a href="#extended_profile_tab" data-toggle="tab" className="nav-link">Extended Profile</a></li>
                      <li className="nav-item"><a href="#emergency_contacts_tab" data-toggle="tab" className="nav-link">Emergency Contacts </a></li>
                      <li className="nav-item"><a href="#employment_information_tab" data-toggle="tab" className="nav-link">Employment Information</a></li>
                      <li className="nav-item"><a href="#skills_tab" data-toggle="tab" className="nav-link">Skills</a></li>
                      <li className="nav-item"><a href="#consents_awards_tab" data-toggle="tab" className="nav-link">Consents and Awards</a></li>
                      <li className="nav-item"><a href="#documents_tab" data-toggle="tab" className="nav-link">Documents</a></li>
                      
                    </ul>
                  </div>
                </div>
              </div>

              <div className="tab-content">
                {/* //============ Profile Contact Tab ============ */}
                <div id="profile_tab" className="pro-overview tab-pane fade show active">
                  <div className="row">
                    <div className="col-md-6 d-flex">
                      <div className="card profile-box flex-fill">
                        <div className="card-body">
                          <h3 className="card-title">Profile <a href="#" className="edit-icon" data-toggle="modal" data-target="#ProfileTab_profile_modal"><i className="fa fa-pencil" /></a></h3>
                          <ul className="personal-info">
                            <li>
                              <div className="title">Preferred Name</div>
                              <div className="text">John</div>
                            </li>
                            <li>
                              <div className="title">First Name</div>
                              <div className="text">Johnathan</div>
                            </li>
                            <li>
                              <div className="title">Middle Name</div>
                              <div className="text">Joe</div>
                            </li>
                            <li>
                              <div className="title">Last Name</div>
                              <div className="text">Smith</div>
                            </li>
                            <li>
                              <div className="title">Date of Birth</div>
                              <div className="text">01-02-1990</div>
                            </li>
                            <li>
                              <div className="title">Gender</div>
                              <div className="text">M</div>
                            </li>
                            <li>
                              <div className="title">Marital Status</div>
                              <div className="text">Single</div>
                            </li>
                            <li>
                              <div className="title">Preferred Language</div>
                              <div className="text">English</div>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                    <div className="col-md-6 d-flex">
                      <div className="card profile-box flex-fill">
                        <div className="card-body">
                          <h3 className="card-title">Contact <a href="#" className="edit-icon" data-toggle="modal" data-target="#ProfileTab_contact_modal"><i className="fa fa-pencil" /></a></h3>
                          {/*<h5 className="section-title">Primary</h5>*/}
                          <ul className="personal-info">
                            <li>
                              <div className="title">Preferred Method</div>
                              <div className="text">Cell Phone</div>
                            </li>
                            <li>
                              <div className="title">PHHS Email</div>
                              <div className="text">Marshall@PHHS.com</div>
                            </li>
                            <li>
                              <div className="title">Personal Email</div>
                              <div className="text">Marshall@gmail.com</div>
                            </li>
                            <li>
                              <div className="title">Cell Phone </div>
                              <div className="text">519-123-4567</div>
                            </li>
                            <li>
                              <div className="title">Home Phone </div>
                              <div className="text">123-456-7890</div>
                            </li>

                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-md-12 d-flex">
                      <div className="card profile-box flex-fill">
                        <div className="card-body">
                          <h3 className="card-title">Address Informations <a href="#" className="edit-icon" data-toggle="modal" data-target="#ProfileTab_address_info_modal"><i className="fa fa-pencil" /></a></h3>
                          <div className="table-responsive">
                            <table className="table table-nowrap">
                              <thead>
                                <tr>
                                  <th>Address Type</th>
                                  <th>Street #</th>
                                  <th>Unit #</th>
                                  <th>Stree Name</th>
                                  <th>Postal Code</th>
                                  <th>City</th>
                                  <th>Province</th>
                                  <th>Country</th>
                                  <th>Action</th>
                                  <th />
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <td>Primary</td>
                                  <td>123</td>
                                  <td>1</td>
                                  <td>Corner St</td>
                                  <td>101 102</td>
                                  <td>London</td>
                                  <td>Ontario</td>
                                  <td>Canada</td>
                                  <td className="text-right">
                                    <div className="dropdown dropdown-action">
                                      <a aria-expanded="false" data-toggle="dropdown" className="action-icon dropdown-toggle" href="#"><i className="material-icons">more_vert</i></a>
                                      <div className="dropdown-menu dropdown-menu-right">
                                        <a href="#" className="dropdown-item"><i className="fa fa-pencil m-r-5" /> Edit</a>
                                        <a href="#" className="dropdown-item"><i className="fa fa-trash-o m-r-5" /> Delete</a>
                                      </div>
                                    </div>
                                  </td>
                                </tr>
                                <tr>
                                  <td>Alternate</td>
                                  <td>987</td>
                                  <td></td>
                                  <td>Highway Rd</td>
                                  <td>1R1 2R2 </td>
                                  <td>London</td>
                                  <td>Ontario</td>
                                  <td>Canada</td>
                                  <td className="text-right">
                                    <div className="dropdown dropdown-action">
                                      <a aria-expanded="false" data-toggle="dropdown" className="action-icon dropdown-toggle" href="#"><i className="material-icons">more_vert</i></a>
                                      <div className="dropdown-menu dropdown-menu-right">
                                        <a href="#" className="dropdown-item"><i className="fa fa-pencil m-r-5" /> Edit</a>
                                        <a href="#" className="dropdown-item"><i className="fa fa-trash-o m-r-5" /> Delete</a>
                                      </div>
                                    </div>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                {/* //============ Profile Contact Tab ============ */}

                {/* //============ Extended Profile Tab ============ */}
                <div id="extended_profile_tab" className="tab-pane fade">
                  <div className="row">
                    <div className="col-md-12 d-flex">
                      <div className="card profile-box flex-fill">
                        <div className="card-body">
                          <h3 className="card-title">Personal Information<a href="#" className="edit-icon" data-toggle="modal" data-target="#ExtendedProfileTab_personal_info_modal"><i className="fa fa-pencil" /></a></h3>
                          <ul className="personal-info">
                            <li>
                              <div className="title">Date of Birth</div>
                              <div className="text">xxxxxxx</div>
                            </li>
                            <li>
                              <div className="title">Marital Status</div>
                              <div className="text">xxxx</div>
                            </li>
                            <li>
                              <div className="title">Preferred Language</div>
                              <div className="text">xxxxxxxxxx</div>
                            </li>
                            <li>
                              <div className="title">Personal Email Address</div>
                              <div className="text">xxxxxxxxxxxxxxxxx</div>
                            </li>
                            <li>
                              <div className="title">SIN</div>
                              <div className="text">000-000-000</div>
                            </li>
                            <li>
                              <div className="title">SIN Expiry Date</div>
                              <div className="text">Not Applicable</div>
                            </li>
                            <li>
                              <div className="title">Work Permit#</div>
                              <div className="text">123</div>
                            </li>
                            <li>
                              <div className="title">Work Permit Expiry</div>
                              <div className="text">01-01-2021</div>
                            </li>
                            <li>
                              <div className="title">OHIP Coverage</div>
                              <div className="text">Yes</div>
                            </li>
                            <li>
                              <div className="title">Health Number</div>
                              <div className="text">123456789</div>
                            </li>
                            <li>
                              <div className="title">Health Card Expiry</div>
                              <div className="text">01-01-2030</div>
                            </li>
                            <li>
                              <div className="title">Other Coverage</div>
                              <div className="text">No</div>
                            </li>
                            <li>
                              <div className="title">Insurer Name</div>
                              <div className="text">Not Applicable</div>
                            </li>
                            <li>
                              <div className="title">Coverage Number</div>
                              <div className="text">Not Applicable</div>
                            </li>
                            <li>
                              <div className="title">Pay Rate</div>
                              <div className="text">$1.00</div>
                            </li>
                            <li>
                              <div className="title">Salary</div>
                              <div className="text"></div>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-md-12 d-flex">
                      <div className="card profile-box flex-fill">
                        <div className="card-body">
                          <h3 className="card-title">PHSS Employment Information<a href="#" className="edit-icon" data-toggle="modal" data-target="#ExtendedProfileTab_phss_employment_info_modal"><i className="fa fa-pencil" /></a></h3>
                          <ul className="personal-info">
                            <li>
                              <div className="title">Staff Number/ID</div>
                              <div className="text">0100</div>
                            </li>
                            <li>
                              <div className="title">Primary Location</div>
                              <div className="text">Belgrave</div>
                            </li>
                            <li>
                              <div className="title">Employment Status</div>
                              <div className="text">Active</div>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                {/* //============ Extended Profile Tab ============ */}

                {/* //=========== Emergency Contacts Tab =========== */}
                <div id="emergency_contacts_tab" className="tab-pane fade">
                  <div className="row">
                    <div className="col-md-12 d-flex">
                      <div className="card profile-box flex-fill">
                        <div className="card-body">
                          <h3 className="card-title">Emergency Contact<a href="#" className="edit-icon" data-toggle="modal" data-target="#EmergencyContacts_emergency_contacts_modal"><i className="fa fa-pencil" /></a></h3>
                          <div className="table-responsive">
                            <table className="table table-nowrap">
                              <thead>
                                <tr>
                                  <th>Emergency Contact Type</th>
                                  <th>First Name</th>
                                  <th>Last Name</th>
                                  <th>Relationship</th>
                                  <th>Phone Number</th>
                                  <th>Alternate Phone</th>
                                  <th>Email</th>
                                  <th>Action</th>
                                  <th />
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <td>Primary</td>
                                  <td>Tom</td>
                                  <td>Cruise</td>
                                  <td>Father</td>
                                  <td>519-123-4567</td>
                                  <td>123-456-7890</td>
                                  <td>TomCruise@Gmail.com</td>
                                  <td className="text-right">
                                    <div className="dropdown dropdown-action">
                                      <a aria-expanded="false" data-toggle="dropdown" className="action-icon dropdown-toggle" href="#"><i className="material-icons">more_vert</i></a>
                                      <div className="dropdown-menu dropdown-menu-right">
                                        <a href="#" className="dropdown-item"><i className="fa fa-pencil m-r-5" /> Edit</a>
                                        <a href="#" className="dropdown-item"><i className="fa fa-trash-o m-r-5" /> Delete</a>
                                      </div>
                                    </div>
                                  </td>
                                </tr>
                                <tr>
                                  <td>Secondary</td>
                                  <td>Lady</td>
                                  <td>Gaga</td>
                                  <td>Mother</td>
                                  <td>123-456-0000</td>
                                  <td>111-222-9876</td>
                                  <td>Lady@Gaga.com</td>
                                  <td className="text-right">
                                    <div className="dropdown dropdown-action">
                                      <a aria-expanded="false" data-toggle="dropdown" className="action-icon dropdown-toggle" href="#"><i className="material-icons">more_vert</i></a>
                                      <div className="dropdown-menu dropdown-menu-right">
                                        <a href="#" className="dropdown-item"><i className="fa fa-pencil m-r-5" /> Edit</a>
                                        <a href="#" className="dropdown-item"><i className="fa fa-trash-o m-r-5" /> Delete</a>
                                      </div>
                                    </div>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                {/* //============ Emergency Contacts Tab ============ */}

                {/* ============ Employment Information infomation Tab ============ */}
                <div id="employment_information_tab" className="tab-pane fade">
                  <div className="row">
                    <div className="col-md-12 d-flex">
                      <div className="card profile-box flex-fill">
                        <div className="card-body">
                          <h3 className="card-title">PHSS Employment History<a href="#" className="edit-icon" data-toggle="modal" data-target="#EmploymentInformation_PHSS_employment_history_modal"><i className="fa fa-pencil" /></a></h3>
                          <div className="table-responsive">
                            <table className="table table-nowrap">
                              <thead>
                                <tr>
                                  <th>Status</th>
                                  <th>Job Title</th>
                                  <th>Location</th>
                                  <th>Employment Type</th>
                                  <th>Employment Hours</th>
                                  <th>Part-time type</th>
                                  <th>Start Date</th>
                                  <th>End Date</th>
                                  <th>Action</th>
                                  <th />
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <td>Active</td>
                                  <td>Staff</td>
                                  <td>Wistow</td>
                                  <td>Permanent</td>
                                  <td>Part-time</td>
                                  <td>Over 24</td>
                                  <td>01-01-2021</td>
                                  <td>01-01-2021</td>
                                  <td className="text-right">
                                    <div className="dropdown dropdown-action">
                                      <a aria-expanded="false" data-toggle="dropdown" className="action-icon dropdown-toggle" href="#"><i className="material-icons">more_vert</i></a>
                                      <div className="dropdown-menu dropdown-menu-right">
                                        <a href="#" className="dropdown-item"><i className="fa fa-pencil m-r-5" /> Edit</a>
                                        <a href="#" className="dropdown-item"><i className="fa fa-trash-o m-r-5" /> Delete</a>
                                      </div>
                                    </div>
                                  </td>
                                </tr>
                                <tr>
                                  <td>Inactive</td>
                                  <td>Janitor</td>
                                  <td>Office</td>
                                  <td>Temporary</td>
                                  <td>Full-Time</td>
                                  <td>Not Applicable</td>
                                  <td>01-01-2020</td>
                                  <td>01-01-2020</td>
                                  <td className="text-right">
                                    <div className="dropdown dropdown-action">
                                      <a aria-expanded="false" data-toggle="dropdown" className="action-icon dropdown-toggle" href="#"><i className="material-icons">more_vert</i></a>
                                      <div className="dropdown-menu dropdown-menu-right">
                                        <a href="#" className="dropdown-item"><i className="fa fa-pencil m-r-5" /> Edit</a>
                                        <a href="#" className="dropdown-item"><i className="fa fa-trash-o m-r-5" /> Delete</a>
                                      </div>
                                    </div>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-md-12 d-flex">
                      <div className="card profile-box flex-fill">
                        <div className="card-body">
                          <h3 className="card-title">Trained Locations<a href="#" className="edit-icon" data-toggle="modal" data-target="#EmploymentInformation_trained_locations_modal"><i className="fa fa-pencil" /></a></h3>
                          <div className="table-responsive">
                            <table className="table table-nowrap">
                              <thead>
                                <tr>
                                  <th>Trained/Worked</th>
                                  <th>Location</th>
                                  <th>Training Type</th>
                                  <th>Date Completed</th>
                                  <th>Date Last Worked</th>
                                  <th># of Hrs Worked</th>
                                  <th>Action</th>
                                  <th />
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <td>Trained</td>
                                  <td>Belgrave</td>
                                  <td></td>
                                  <td>01-01-0001</td>
                                  <td>01-02-2020</td>
                                  <td>200</td>
                                  <td className="text-right">
                                    <div className="dropdown dropdown-action">
                                      <a aria-expanded="false" data-toggle="dropdown" className="action-icon dropdown-toggle" href="#"><i className="material-icons">more_vert</i></a>
                                      <div className="dropdown-menu dropdown-menu-right">
                                        <a href="#" className="dropdown-item"><i className="fa fa-pencil m-r-5" /> Edit</a>
                                        <a href="#" className="dropdown-item"><i className="fa fa-trash-o m-r-5" /> Delete</a>
                                      </div>
                                    </div>
                                  </td>
                                </tr>
                                <tr>
                                  <td>Trained</td>
                                  <td>Wistow</td>
                                  <td></td>
                                  <td>01-01-0002</td>
                                  <td>01-02-1999</td>
                                  <td>100</td>
                                  <td className="text-right">
                                    <div className="dropdown dropdown-action">
                                      <a aria-expanded="false" data-toggle="dropdown" className="action-icon dropdown-toggle" href="#"><i className="material-icons">more_vert</i></a>
                                      <div className="dropdown-menu dropdown-menu-right">
                                        <a href="#" className="dropdown-item"><i className="fa fa-pencil m-r-5" /> Edit</a>
                                        <a href="#" className="dropdown-item"><i className="fa fa-trash-o m-r-5" /> Delete</a>
                                      </div>
                                    </div>
                                  </td>
                                </tr>
                                <tr>
                                  <td>Worked</td>
                                  <td>Cranbrook</td>
                                  <td></td>
                                  <td></td>
                                  <td>01-02-2020</td>
                                  <td>50</td>
                                  <td className="text-right">
                                    <div className="dropdown dropdown-action">
                                      <a aria-expanded="false" data-toggle="dropdown" className="action-icon dropdown-toggle" href="#"><i className="material-icons">more_vert</i></a>
                                      <div className="dropdown-menu dropdown-menu-right">
                                        <a href="#" className="dropdown-item"><i className="fa fa-pencil m-r-5" /> Edit</a>
                                        <a href="#" className="dropdown-item"><i className="fa fa-trash-o m-r-5" /> Delete</a>
                                      </div>
                                    </div>
                                  </td>
                                </tr>
                                <tr>
                                  <td>Worked</td>
                                  <td>Clarke Road</td>
                                  <td></td>
                                  <td></td>
                                  <td>01-02-1999</td>
                                  <td>100</td>
                                  <td className="text-right">
                                    <div className="dropdown dropdown-action">
                                      <a aria-expanded="false" data-toggle="dropdown" className="action-icon dropdown-toggle" href="#"><i className="material-icons">more_vert</i></a>
                                      <div className="dropdown-menu dropdown-menu-right">
                                        <a href="#" className="dropdown-item"><i className="fa fa-pencil m-r-5" /> Edit</a>
                                        <a href="#" className="dropdown-item"><i className="fa fa-trash-o m-r-5" /> Delete</a>
                                      </div>
                                    </div>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                {/* //============ Employment infomation Tab ============ */}

                {/* ============ Skills infomation Tab ============ */}
                <div id="skills_tab" className="tab-pane fade">
                  <div className="row">
                    <div className="col-md-12 d-flex">
                      <div className="card profile-box flex-fill">
                        <div className="card-body">
                          <h3 className="card-title">Skills<a href="#" className="edit-icon" data-toggle="modal" data-target="#Skills_skills_modal"><i className="fa fa-pencil" /></a></h3>
                          <div className="table-responsive">
                            <table className="table table-nowrap">
                              <thead>
                                <tr>
                                  <th>Category</th>
                                  <th>SubCategory</th>
                                  <th>Start Date</th>
                                  <th>End Date</th>
                                  <th>Action</th>
                                  <th />
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <td>Licenses and Permits</td>
                                  <td>CPI</td>
                                  <td>01-01-2000</td>
                                  <td>01-02-2004</td>
                                  <td className="text-right">
                                    <div className="dropdown dropdown-action">
                                      <a aria-expanded="false" data-toggle="dropdown" className="action-icon dropdown-toggle" href="#"><i className="material-icons">more_vert</i></a>
                                      <div className="dropdown-menu dropdown-menu-right">
                                        <a href="#" className="dropdown-item"><i className="fa fa-pencil m-r-5" /> Edit</a>
                                        <a href="#" className="dropdown-item"><i className="fa fa-trash-o m-r-5" /> Delete</a>
                                      </div>
                                    </div>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                {/* //============ Skills infomation Tab ============ */}

                {/* ============ Consents and Awards Tab ============ */}
                <div id="consents_awards_tab" className="tab-pane fade">
                  <div className="row">
                    <div className="col-md-12 d-flex">
                      <div className="card profile-box flex-fill">
                        <div className="card-body">
                          <h3 className="card-title">Consents and Waivers<a href="#" className="edit-icon" data-toggle="modal" data-target="#ConsentsAwards_Consents_waivers_modal"><i className="fa fa-pencil" /></a></h3>
                          <div className="table-responsive">
                            <table className="table table-nowrap">
                              <thead>
                                <tr>
                                  <th>Name</th>
                                  <th>Description</th>
                                  <th>Issue Date</th>
                                  <th>Expiry Date</th>
                                  <th>Status</th>
                                  <th>Attachment</th>
                                  <th>Action</th>
                                  <th />
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <td>Photo Release</td>
                                  <td>Permission to use photos on website</td>
                                  <td>01-01-2021</td>
                                  <td>01-02-2030</td>
                                  <td>Active</td>
                                  <td></td>
                                  <td className="text-right">
                                    <div className="dropdown dropdown-action">
                                      <a aria-expanded="false" data-toggle="dropdown" className="action-icon dropdown-toggle" href="#"><i className="material-icons">more_vert</i></a>
                                      <div className="dropdown-menu dropdown-menu-right">
                                        <a href="#" className="dropdown-item"><i className="fa fa-pencil m-r-5" /> Edit</a>
                                        <a href="#" className="dropdown-item"><i className="fa fa-trash-o m-r-5" /> Delete</a>
                                      </div>
                                    </div>
                                  </td>
                                </tr>
                                <tr>
                                  <td>Consent ABC</td>
                                  <td>Consenting to..</td>
                                  <td>01-01-2003</td>
                                  <td>01-02-2050</td>
                                  <td>Active</td>
                                  <td></td>
                                  <td className="text-right">
                                    <div className="dropdown dropdown-action">
                                      <a aria-expanded="false" data-toggle="dropdown" className="action-icon dropdown-toggle" href="#"><i className="material-icons">more_vert</i></a>
                                      <div className="dropdown-menu dropdown-menu-right">
                                        <a href="#" className="dropdown-item"><i className="fa fa-pencil m-r-5" /> Edit</a>
                                        <a href="#" className="dropdown-item"><i className="fa fa-trash-o m-r-5" /> Delete</a>
                                      </div>
                                    </div>
                                  </td>
                                </tr>
                                <tr>
                                  <td>Waiver ABC</td>
                                  <td>Permission</td>
                                  <td>01-01-1999</td>
                                  <td>01-02-2005</td>
                                  <td>Expired</td>
                                  <td></td>
                                  <td className="text-right">
                                    <div className="dropdown dropdown-action">
                                      <a aria-expanded="false" data-toggle="dropdown" className="action-icon dropdown-toggle" href="#"><i className="material-icons">more_vert</i></a>
                                      <div className="dropdown-menu dropdown-menu-right">
                                        <a href="#" className="dropdown-item"><i className="fa fa-pencil m-r-5" /> Edit</a>
                                        <a href="#" className="dropdown-item"><i className="fa fa-trash-o m-r-5" /> Delete</a>
                                      </div>
                                    </div>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-md-12 d-flex">
                      <div className="card profile-box flex-fill">
                        <div className="card-body">
                          <h3 className="card-title">Awards and Recognition<a href="#" className="edit-icon" data-toggle="modal" data-target="#ConsentsAwards_awards_recognition_modal"><i className="fa fa-pencil" /></a></h3>
                          <div className="table-responsive">
                            <table className="table table-nowrap">
                              <thead>
                                <tr>
                                  <th>Award Name</th>
                                  <th>Issuing Organization</th>
                                  <th>Issue Date</th>
                                  <th>Action</th>
                                  <th />
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <td>MVP</td>
                                  <td>Canada</td>
                                  <td>01-02-2003</td>
                                  <td className="text-right">
                                    <div className="dropdown dropdown-action">
                                      <a aria-expanded="false" data-toggle="dropdown" className="action-icon dropdown-toggle" href="#"><i className="material-icons">more_vert</i></a>
                                      <div className="dropdown-menu dropdown-menu-right">
                                        <a href="#" className="dropdown-item"><i className="fa fa-pencil m-r-5" /> Edit</a>
                                        <a href="#" className="dropdown-item"><i className="fa fa-trash-o m-r-5" /> Delete</a>
                                      </div>
                                    </div>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                {/* //============ Consents and Awards Tab ============ */}

                {/* ============ Documents Tab ============ */}
                <div id="documents_tab" className="tab-pane fade">
                  <div className="row">
                    <div className="col-md-12 d-flex">
                      <div className="card profile-box flex-fill">
                        <div className="card-body">
                          <h3 className="card-title">Documents<a href="#" className="edit-icon" data-toggle="modal" data-target="#Documents_documents_modal"><i className="fa fa-pencil" /></a></h3>
                          <div className="table-responsive">
                            <table className="table table-nowrap">
                              <thead>
                                <tr>
                                  <th>Name</th>
                                  <th>Description</th>
                                  <th>Date added</th>
                                  <th>Attachment</th>
                                  <th>Action</th>
                                  <th />
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <td>Resume 2019</td>
                                  <td>Resume from 2019</td>
                                  <td>01-01-2021</td>
                                  <td></td>
                                  <td className="text-right">
                                    <div className="dropdown dropdown-action">
                                      <a aria-expanded="false" data-toggle="dropdown" className="action-icon dropdown-toggle" href="#"><i className="material-icons">more_vert</i></a>
                                      <div className="dropdown-menu dropdown-menu-right">
                                        <a href="#" className="dropdown-item"><i className="fa fa-pencil m-r-5" /> Edit</a>
                                        <a href="#" className="dropdown-item"><i className="fa fa-trash-o m-r-5" /> Delete</a>
                                      </div>
                                    </div>
                                  </td>
                                </tr>
                                <tr>
                                  <td>References</td>
                                  <td>Professional References</td>
                                  <td>01-01-2003</td>
                                  <td></td>
                                  <td className="text-right">
                                    <div className="dropdown dropdown-action">
                                      <a aria-expanded="false" data-toggle="dropdown" className="action-icon dropdown-toggle" href="#"><i className="material-icons">more_vert</i></a>
                                      <div className="dropdown-menu dropdown-menu-right">
                                        <a href="#" className="dropdown-item"><i className="fa fa-pencil m-r-5" /> Edit</a>
                                        <a href="#" className="dropdown-item"><i className="fa fa-trash-o m-r-5" /> Delete</a>
                                      </div>
                                    </div>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                {/* //============ Documents Tab ============ */}

              </div>

            </div>
            {/* //Page Content */}


            {/* ------------------------- Modes ------------------------- */}

            {/* Profile Modal */}
            <div id="profile_info" className="modal custom-modal fade" role="dialog">
              <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div className="modal-content">
                  <div className="modal-header">
                    <h5 className="modal-title">Profile Information</h5>
                    <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">×</span>
                    </button>
                  </div>
                  <div className="modal-body">
                    <form>
                      <div className="row">
                        <div className="col-md-12">
                          <div className="profile-img-wrap edit-img">
                            <img className="inline-block" src={Avatar_02} alt="user" />
                            <div className="fileupload btn">
                              <span className="btn-text">edit</span>
                              <input className="upload" type="file" />
                            </div>
                          </div>
                          <div className="row">
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>First Name</label>
                                <input type="text" className="form-control" defaultValue="John" />
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Last Name</label>
                                <input type="text" className="form-control" defaultValue="Doe" />
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Birth Date</label>
                                <div className="cal-icon">
                                  <input className="form-control datetimepicker" type="text" defaultValue="05/06/1985" />
                                </div>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Gender</label>
                                <select className="select form-control">
                                  <option value="male selected">Male</option>
                                  <option value="female">Female</option>
                                </select>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-md-12">
                          <div className="form-group">
                            <label>Address</label>
                            <input type="text" className="form-control" defaultValue="4487 Snowbird Lane" />
                          </div>
                        </div>
                        <div className="col-md-6">
                          <div className="form-group">
                            <label>State</label>
                            <input type="text" className="form-control" defaultValue="New York" />
                          </div>
                        </div>
                        <div className="col-md-6">
                          <div className="form-group">
                            <label>Country</label>
                            <input type="text" className="form-control" defaultValue="United States" />
                          </div>
                        </div>
                        <div className="col-md-6">
                          <div className="form-group">
                            <label>Pin Code</label>
                            <input type="text" className="form-control" defaultValue={10523} />
                          </div>
                        </div>
                        <div className="col-md-6">
                          <div className="form-group">
                            <label>Phone Number</label>
                            <input type="text" className="form-control" defaultValue="631-889-3206" />
                          </div>
                        </div>
                        <div className="col-md-6">
                          <div className="form-group">
                            <label>Department <span className="text-danger">*</span></label>
                            <select className="select">
                              <option>Select Department</option>
                              <option>Web Development</option>
                              <option>IT Management</option>
                              <option>Marketing</option>
                            </select>
                          </div>
                        </div>
                        <div className="col-md-6">
                          <div className="form-group">
                            <label>Designation <span className="text-danger">*</span></label>
                            <select className="select">
                              <option>Select Designation</option>
                              <option>Web Designer</option>
                              <option>Web Developer</option>
                              <option>Android Developer</option>
                            </select>
                          </div>
                        </div>
                        <div className="col-md-6">
                          <div className="form-group">
                            <label>Reports To <span className="text-danger">*</span></label>
                            <select className="select">
                              <option>-</option>
                              <option>Wilmer Deluna</option>
                              <option>Lesley Grauer</option>
                              <option>Jeffery Lalor</option>
                            </select>
                          </div>
                        </div>
                      </div>
                      <div className="submit-section">
                        <button className="btn btn-primary submit-btn">Submit</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
            {/* /Profile Modal */}

            {/* ****************** Profile Tab Modals ****************** */}
            {/* Staff Profile Info Modal */}
            <div id="ProfileTab_profile_modal" className="modal custom-modal fade" role="dialog">
              <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div className="modal-content">
                  <div className="modal-header">
                    <h5 className="modal-title">Staff Profile Information</h5>
                    <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">×</span>
                    </button>
                  </div>
                  <div className="modal-body">
                    <form>
                      <div className="card">
                        <div className="card-body">
                          <div className="row">
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Preferred Name<span className="text-danger">*</span></label>
                                <input type="text" className="form-control" />
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Date of Birth<span className="text-danger">*</span></label>
                                <div className="cal-icon">
                                  <input className="form-control datetimepicker" type="text" />
                                </div>
                              </div>
                            </div>
                            <div className="col-md-4">
                              <div className="form-group">
                                <label>First Name<span className="text-danger">*</span></label>
                                <input className="form-control" type="text" />
                              </div>
                            </div>
                            <div className="col-md-4">
                              <div className="form-group">
                                <label>Middle Name<span className="text-danger">*</span></label>
                                <div className="form-group">
                                  <input className="form-control" type="text" />
                                </div>
                              </div>
                            </div>
                            <div className="col-md-4">
                              <div className="form-group">
                                <label>Last Name<span className="text-danger">*</span></label>
                                <input className="form-control" type="text" />
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Gender<span className="text-danger">*</span></label>
                                <select className="select form-control">
                                  <option>-</option>
                                  <option>Male</option>
                                  <option>Female</option>
                                </select>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Marital status <span className="text-danger">*</span></label>
                                <select className="select form-control">
                                  <option>-</option>
                                  <option>Single</option>
                                  <option>Married</option>
                                </select>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Preferred Language<span className="text-danger">*</span></label>
                                <select className="select form-control">
                                  <option>-</option>
                                  <option>English</option>
                                  <option>French</option>
                                </select>
                              </div>
                            </div>
                          </div>
                          <div className="submit-section">
                            <button className="btn btn-primary submit-btn">Submit</button>
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
            {/* /Staff Profile Info Modal */}

            {/* Staff Contact Information Modal */}
            <div id="ProfileTab_contact_modal" className="modal custom-modal fade" role="dialog">
              <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div className="modal-content">
                  <div className="modal-header">
                    <h5 className="modal-title">Contact Information 123</h5>
                    <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">×</span>
                    </button>
                  </div>
                  <div className="modal-body">
                    <form>
                      <div className="card">
                        <div className="card-body">
                          {/* <h3 className="card-title">Primary Contact</h3> */}
                          <div className="row">
                            <div className="col-md-12">
                              <div className="form-group">
                                <label>Preferred Method<span className="text-danger">*</span></label>
                                <input type="text" className="form-control" />
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Personal Email <span className="text-danger">*</span></label>
                                <input className="form-control" type="text" />
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>PHSS Email <span className="text-danger">*</span></label>
                                <input className="form-control" type="text" />
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Cell Phone <span className="text-danger">*</span></label>
                                <input className="form-control" type="text" />
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Home Phone <span className="text-danger">*</span></label>
                                <input className="form-control" type="text" />
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      {/*
                      <div className="card">
                        <div className="card-body">
                          <h3 className="card-title">Primary Contact</h3>
                          <div className="row">
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Name <span className="text-danger">*</span></label>
                                <input type="text" className="form-control" />
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Relationship <span className="text-danger">*</span></label>
                                <input className="form-control" type="text" />
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Phone <span className="text-danger">*</span></label>
                                <input className="form-control" type="text" />
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Phone 2</label>
                                <input className="form-control" type="text" />
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      */}
                      <div className="submit-section">
                        <button className="btn btn-primary submit-btn">Submit</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
            {/* //Staff Contact Information Modal */}

            {/* Address Informations Modal */}
            <div id="ProfileTab_address_info_modal" className="modal custom-modal fade" role="dialog">
              <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div className="modal-content">
                  <div className="modal-header">
                    <h5 className="modal-title">Address Informations</h5>
                    <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">×</span>
                    </button>
                  </div>
                  <div className="modal-body">
                    <form>
                      <div className="card">
                        <div className="card-body">
                          <div className="row">
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Address Type <span className="text-danger">*</span></label>
                                <select className="select form-control">
                                  <option>-</option>
                                  <option>Primary</option>
                                  <option>Local</option>
                                </select>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Street # <span className="text-danger">*</span></label>
                                <div className="cal-icon">
                                  <input className="form-control" type="text" />
                                </div>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Unit # <span className="text-danger">*</span></label>
                                <input className="form-control" type="text" />
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Street Name <span className="text-danger">*</span></label>
                                <input className="form-control" type="text" />
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>City <span className="text-danger">*</span></label>
                                <div className="form-group">
                                  <input className="form-control" type="text" />
                                </div>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Postal Code <span className="text-danger">*</span></label>
                                <input className="form-control" type="text" value="123-456-7890"/>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Province <span className="text-danger">*</span></label>
                                <select className="select form-control">
                                  <option>-</option>
                                  <option>Province 1</option>
                                  <option>Province 2</option>
                                </select>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Country <span className="text-danger">*</span></label>
                                <input className="form-control" type="text" />
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Start Date</label>
                                <div className="cal-icon">
                                  <input className="form-control datetimepicker" type="text" />
                                </div>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Status <span className="text-danger">*</span></label>
                                <input className="form-control" type="text" />
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>End Date</label>
                                <div className="cal-icon">
                                  <input className="form-control datetimepicker" type="text" />
                                </div>
                              </div>
                            </div>
                          </div>
                          <div className="submit-section">
                            <button className="btn btn-primary submit-btn">Submit</button>
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
            {/* //Address Informations Modal */}
            {/* ****************** Profile Tab Modals ****************** */}

            {/* ************* Extended Profile Tab Modals ************** */}
            {/* Extended Profile Modal */}
            <div id="ExtendedProfileTab_personal_info_modal" className="modal custom-modal fade" role="dialog">
              <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div className="modal-content">
                  <div className="modal-header">
                    <h5 className="modal-title">Personal Information</h5>
                    <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">×</span>
                    </button>
                  </div>
                  <div className="modal-body">
                    <form>
                      <div className="card">
                        <div className="card-body">
                          <div className="row">
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Date of Birth<span className="text-danger">*</span></label>
                                <div className="cal-icon">
                                  <input type="text" className="form-control datetimepicker" />
                                </div>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Marital Status<span className="text-danger">*</span></label>
                                <select className="select form-control">
                                  <option>-</option>
                                  <option>Single</option>
                                  <option>Married</option>
                                </select>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Preferred Language<span className="text-danger">*</span></label>
                                <select className="select form-control">
                                  <option>-</option>
                                  <option>English</option>
                                  <option>French </option>
                                </select>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Personal Email Address<span className="text-danger">*</span></label>
                                <div className="form-group">
                                  <input className="form-control" type="text" />
                                </div>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>SIN<span className="text-danger">*</span></label>
                                <input className="form-control" type="text" />
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>SIN Expiry Date<span className="text-danger">*</span></label>
                                <div className="cal-icon">
                                  <input type="text" className="form-control datetimepicker" />
                                </div>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Work Permit#<span className="text-danger">*</span></label>
                                <input className="form-control" type="text" />
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Work Permit Expiry<span className="text-danger">*</span></label>
                                <div className="cal-icon">
                                  <input type="text" className="form-control datetimepicker" />
                                </div>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>OHIP Coverage<span className="text-danger">*</span></label>
                                <select className="select form-control">
                                  <option>-</option>
                                  <option>Yes</option>
                                  <option>No</option>
                                </select>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Health Number<span className="text-danger">*</span></label>
                                <input type="text" className="form-control" />
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Health Card Expiry<span className="text-danger">*</span></label>
                                <div className="cal-icon">
                                  <input type="text" className="form-control datetimepicker" />
                                </div>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Other Coverage<span className="text-danger">*</span></label>
                                <select className="select form-control">
                                  <option>-</option>
                                  <option>Yes</option>
                                  <option>No</option>
                                </select>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Insurer Name<span className="text-danger">*</span></label>
                                <input type="text" className="form-control" />
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Coverage Number<span className="text-danger">*</span></label>
                                <input type="text" className="form-control" />
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Pay Rate<span className="text-danger">*</span></label>
                                <input type="text" className="form-control" />
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Salary<span className="text-danger">*</span></label>
                                <input type="text" className="form-control" />
                              </div>
                            </div>
                          </div>
                          <div className="submit-section">
                            <button className="btn btn-primary submit-btn">Submit</button>
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
            {/* /Extended Profile Modal */}

            {/* PHSS Employment Information Modal */}
            <div id="ExtendedProfileTab_phss_employment_info_modal" className="modal custom-modal fade" role="dialog">
              <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div className="modal-content">
                  <div className="modal-header">
                    <h5 className="modal-title">PHSS Employment Information</h5>
                    <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">×</span>
                    </button>
                  </div>
                  <div className="modal-body">
                    <form>
                      <div className="card">
                        <div className="card-body">
                          <div className="row">
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Staff Number/ID<span className="text-danger">*</span></label>
                                <input type="text" className="form-control" />
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Primary Location<span className="text-danger">*</span></label>
                                <input type="text" className="form-control" />
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Employment Status<span className="text-danger">*</span></label>
                                <select className="select form-control">
                                  <option>-</option>
                                  <option>Active</option>
                                  <option>Inactive </option>
                                </select>
                              </div>
                            </div>
                          </div>
                          <div className="submit-section">
                            <button className="btn btn-primary submit-btn">Submit</button>
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
            {/* /PHSS Employment Information Modal */}
            {/* ************* Extended Profile Tab Modals ************** */}

            {/* ************ Emergency Contacts Tab Modals ************* */}
            {/* Emergency Contacts Modal */}
            <div id="EmergencyContacts_emergency_contacts_modal" className="modal custom-modal fade" role="dialog">
              <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div className="modal-content">
                  <div className="modal-header">
                    <h5 className="modal-title">Emergency Contacts</h5>
                    <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">×</span>
                    </button>
                  </div>
                  <div className="modal-body">
                    <form>
                      <div className="card">
                        <div className="card-body">
                          <div className="row">
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Contact Type<span className="text-danger">*</span></label>
                                <select className="select form-control">
                                  <option>-</option>
                                  <option>Primary</option>
                                  <option>Secondary</option>
                                </select>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>First Name<span className="text-danger">*</span></label>
                                <input type="text" className="form-control" />
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Last Name<span className="text-danger">*</span></label>
                                <input type="text" className="form-control" />
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Relationship<span className="text-danger">*</span></label>
                                <input type="text" className="form-control" />
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Phone Name<span className="text-danger">*</span></label>
                                <input type="text" className="form-control" />
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Alternate Phone<span className="text-danger">*</span></label>
                                <input type="text" className="form-control" />
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Email<span className="text-danger">*</span></label>
                                <input type="text" className="form-control" />
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Notes<span className="text-danger">*</span></label>
                                <input type="text" className="form-control" />
                              </div>
                            </div>
                          </div>
                          <div className="submit-section">
                            <button className="btn btn-primary submit-btn">Submit</button>
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
            {/* /Emergency Contacts Modal */}
            {/* ************ Emergency Contacts Tab Modals ************* */}

            {/* ********** Employment Information Tab Modals *********** */}
            {/* PHSS Employment History Modal */}
            <div id="EmploymentInformation_PHSS_employment_history_modal" className="modal custom-modal fade" role="dialog">
              <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div className="modal-content">
                  <div className="modal-header">
                    <h5 className="modal-title">PHSS Employment History</h5>
                    <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">×</span>
                    </button>
                  </div>
                  <div className="modal-body">
                    <form>
                      <div className="card">
                        <div className="card-body">
                          <div className="row">
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Status<span className="text-danger">*</span></label>
                                <select className="select form-control">
                                  <option>-</option>
                                  <option>Active</option>
                                  <option>Inactive</option>
                                </select>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Job Title<span className="text-danger">*</span></label>
                                <select className="select form-control">
                                  <option>-</option>
                                  <option>Staff</option>
                                  <option>Janitor</option>
                                </select>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Location<span className="text-danger">*</span></label>
                                <input type="text" className="form-control" />
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Employment Type<span className="text-danger">*</span></label>
                                <select className="select form-control">
                                  <option>-</option>
                                  <option>Permanent</option>
                                  <option>Temporary</option>
                                </select>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Employment Hours<span className="text-danger">*</span></label>
                                <select className="select form-control">
                                  <option>-</option>
                                  <option>Part-time</option>
                                  <option>Full-Time</option>
                                </select>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Part-time type<span className="text-danger">*</span></label>
                                <input type="text" className="form-control" />
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Start Date<span className="text-danger">*</span></label>
                                <div className="cal-icon">
                                  <input className="form-control datetimepicker" type="text" defaultValue="05/06/1985" />
                                </div>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>End Date<span className="text-danger">*</span></label>
                                <div className="cal-icon">
                                  <input className="form-control datetimepicker" type="text" defaultValue="05/06/1985" />
                                </div>
                              </div>
                            </div>
                          </div>
                          <div className="submit-section">
                            <button className="btn btn-primary submit-btn">Submit</button>
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
            {/* //PHSS Employment History Modal */}
            {/* Trained Locations Modal */}
            <div id="EmploymentInformation_trained_locations_modal" className="modal custom-modal fade" role="dialog">
              <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div className="modal-content">
                  <div className="modal-header">
                    <h5 className="modal-title">Trained Locations</h5>
                    <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">×</span>
                    </button>
                  </div>
                  <div className="modal-body">
                    <form>
                      <div className="card">
                        <div className="card-body">
                          <div className="row">
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Trained/Worked<span className="text-danger">*</span></label>
                                <select className="select form-control">
                                  <option>-</option>
                                  <option>Trained</option>
                                  <option>Worked</option>
                                </select>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Location<span className="text-danger">*</span></label>
                                <input type="text" className="form-control" />
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Training Type<span className="text-danger">*</span></label>
                                <input type="text" className="form-control" />
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Date Completed Type<span className="text-danger">*</span></label>
                                <div className="cal-icon">
                                  <input className="form-control datetimepicker" type="text" defaultValue="05/06/1985" />
                                </div>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Date Last Worked<span className="text-danger">*</span></label>
                                <div className="cal-icon">
                                  <input className="form-control datetimepicker" type="text" defaultValue="05/06/1985" />
                                </div>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label># of Hrs Worked<span className="text-danger">*</span></label>
                                <input type="text" className="form-control" />
                              </div>
                            </div>
                          </div>
                          <div className="submit-section">
                            <button className="btn btn-primary submit-btn">Submit</button>
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
            {/* //Trained Locations Modal */}
            {/* /********** Employment Information Tab Modals *********** */}

            {/* ****************** Skills Tab Modals ******************* */}
            {/* Skills Modal */}
            <div id="Skills_skills_modal" className="modal custom-modal fade" role="dialog">
              <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div className="modal-content">
                  <div className="modal-header">
                    <h5 className="modal-title">Skills</h5>
                    <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">×</span>
                    </button>
                  </div>
                  <div className="modal-body">
                    <form>
                      <div className="card">
                        <div className="card-body">
                          <div className="row">
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Category<span className="text-danger">*</span></label>
                                <select className="select form-control">
                                  <option>-</option>
                                  <option>Licenses and Permits</option>
                                  <option>Permits and Licenses</option>
                                </select>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>SubCategory<span className="text-danger">*</span></label>
                                <select className="select form-control">
                                  <option>-</option>
                                  <option>CPI 1</option>
                                  <option>CPI 2</option>
                                </select>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Start Date<span className="text-danger">*</span></label>
                                <div className="cal-icon">
                                  <input className="form-control datetimepicker" type="text" defaultValue="05/06/1985" />
                                </div>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>End Date<span className="text-danger">*</span></label>
                                <div className="cal-icon">
                                  <input className="form-control datetimepicker" type="text" defaultValue="05/06/1985" />
                                </div>
                              </div>
                            </div>
                          </div>
                          <div className="submit-section">
                            <button className="btn btn-primary submit-btn">Submit</button>
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
            {/* //Skills Modal */}
            {/* /****************** Skills Tab Modals ******************* */}

            {/* ************ Consents and Awards Tab Modals ************ */}
            {/* Consents and Waivers Modal */}
            <div id="ConsentsAwards_Consents_waivers_modal" className="modal custom-modal fade" role="dialog">
              <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div className="modal-content">
                  <div className="modal-header">
                    <h5 className="modal-title">Consents and Waivers</h5>
                    <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">×</span>
                    </button>
                  </div>
                  <div className="modal-body">
                    <form>
                      <div className="card">
                        <div className="card-body">
                          <div className="row">
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Name<span className="text-danger">*</span></label>
                                <input className="form-control" type="text" defaultValue="05/06/1985" />
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Status<span className="text-danger">*</span></label>
                                <select className="select form-control">
                                  <option>-</option>
                                  <option>Active</option>
                                  <option>Expired</option>
                                </select>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Issue Date<span className="text-danger">*</span></label>
                                <div className="cal-icon">
                                  <input className="form-control datetimepicker" type="text" defaultValue="05/06/1985" />
                                </div>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Expiry Date<span className="text-danger">*</span></label>
                                <div className="cal-icon">
                                  <input className="form-control datetimepicker" type="text" defaultValue="05/06/1985" />
                                </div>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Description<span className="text-danger">*</span></label>
                                <input className="form-control" type="text" />
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Notes<span className="text-danger">*</span></label>
                                <input className="form-control" type="text" />
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Attachment<span className="text-danger">*</span></label>
                                <div className="profile-img-wrap edit-img">
                                  <img className="inline-block" src={Avatar_02} alt="user" />
                                  <div className="fileupload btn">
                                    <span className="btn-text">edit</span>
                                    <input className="upload" type="file" />
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div className="submit-section">
                            <button className="btn btn-primary submit-btn">Submit</button>
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
            {/* //Consents and Awards Modal */}
            {/* Awards and Recognition Modal */}
            <div id="ConsentsAwards_awards_recognition_modal" className="modal custom-modal fade" role="dialog">
              <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div className="modal-content">
                  <div className="modal-header">
                    <h5 className="modal-title">Awards and Recognition</h5>
                    <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">×</span>
                    </button>
                  </div>
                  <div className="modal-body">
                    <form>
                      <div className="card">
                        <div className="card-body">
                          <div className="row">
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Award Name<span className="text-danger">*</span></label>
                                <input className="form-control" type="text" />
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Issuing Organization<span className="text-danger">*</span></label>
                                <select className="select form-control">
                                  <option>-</option>
                                  <option>Canada</option>
                                  <option>Canada</option>
                                </select>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Issue Date<span className="text-danger">*</span></label>
                                <div className="cal-icon">
                                  <input className="form-control datetimepicker" type="text" defaultValue="05/06/1985" />
                                </div>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Notes<span className="text-danger">*</span></label>
                                <input className="form-control" type="text" />
                              </div>
                            </div>
                          </div>
                          <div className="submit-section">
                            <button className="btn btn-primary submit-btn">Submit</button>
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
            {/* //Awards and Recognition Modal */}
            {/* ************ Consents and Waivers Tab Modals ************ */}

            {/* **************** Documents Tab Modals ****************** */}
            {/* Documents Modal */}
            <div id="Documents_documents_modal" className="modal custom-modal fade" role="dialog">
              <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div className="modal-content">
                  <div className="modal-header">
                    <h5 className="modal-title">Documents</h5>
                    <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">×</span>
                    </button>
                  </div>
                  <div className="modal-body">
                    <form>
                      <div className="card">
                        <div className="card-body">
                          <div className="row">
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Name<span className="text-danger">*</span></label>
                                <input className="form-control" type="text" />
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Description<span className="text-danger">*</span></label>
                                <input className="form-control" type="text" />
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Date added<span className="text-danger">*</span></label>
                                <div className="cal-icon">
                                  <input className="form-control datetimepicker" type="text" defaultValue="05/06/1985" />
                                </div>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Attachment<span className="text-danger">*</span></label>
                                <div className="profile-img-wrap edit-img">
                                  <img className="inline-block" src={Avatar_02} alt="user" />
                                  <div className="fileupload btn">
                                    <span className="btn-text">edit</span>
                                    <input className="upload" type="file" />
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div className="submit-section">
                            <button className="btn btn-primary submit-btn">Submit</button>
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
            {/* //Documents Modal */}
            {/* **************** Documents Tab Modals ****************** */}

            {/* ------------------------- Modes ------------------------- */}
          </div>
       
    );
  }
}
