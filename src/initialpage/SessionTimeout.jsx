import React, { useContext, useRef, useState } from "react";
import IdleTimer from "react-idle-timer";

import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import SystemHelpers from './Helpers/SystemHelper';

export default function SessionTimeout() {
  const idleTimer = useRef(null);
  //const {handleLogoutUser} = useContext(AppStateContext);

  const onActive = () => {
    // console.log("active");

    // timer reset automatically.
  };

  const onIdle = () => {
    // console.log("idle")
    //handleLogoutUser();
    LogoutAuth();
  };

  const LogoutAuth = () => {
   
    SystemHelpers.SessionOut();
    //this.props.history.push("/login");
    location.reload();
    SystemHelpers.ToastSuccess('session timeout');
    
    return null;
  }

  return (
    <>
      <IdleTimer
        ref={idleTimer}
        onActive={onActive}
        onIdle={onIdle}
        debounce={250}
        timeout={300000000}
      />
    </>
  );
}