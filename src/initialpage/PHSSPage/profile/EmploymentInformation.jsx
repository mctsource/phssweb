/**
 * Signin Firebase
 */

import React, { Component } from 'react';
import { Helmet } from "react-helmet";

import EmploymentHistory from "./EmploymentInformation/EmploymentHistory";
import TrainedLocations from "./EmploymentInformation/TrainedLocations";

import CryptoAES from 'crypto-js/aes';
import CryptoENC from 'crypto-js/enc-utf8';



class EmploymentInformation extends Component {
constructor(props) {
    super(props);

    this.state = {
        errormsg :  '',
        user_role: [],
        staffContactID:this.props.staffContactID
    };
    this.setPropState = this.setPropState.bind(this);
}

setPropState(key, value) {
      this.setState({ [key]: value });
}

componentDidMount() {
    // console.log("Addlocation1");
    // console.log(this.props.primaryLocationId);
    // console.log("employment_history  course_training");
     
    // console.log(this.props.course_training_can);
    
    // console.log(this.props.employment_history_can_create);
    // console.log(this.props.employment_history_can_update);
    // console.log(this.props.employment_history_can_view);
    // console.log(this.props.employment_history_can_delete);
    // console.log(this.props.employment_history_can_approve);
    // console.log(this.props.employment_history_can_export);

    // console.log(this.props.course_training_can_create);
    // console.log(this.props.course_training_can_update);
    // console.log(this.props.course_training_can_view);
    // console.log(this.props.course_training_can_delete);
    // console.log(this.props.course_training_can_approve);
    // console.log(this.props.course_training_can_export);
}

   render() {
     
      return (
        <div>

          {this.props.employment_history_can.employment_history_can_view == true ?
          <EmploymentHistory 
            staffContactID={this.state.staffContactID}
            primaryLocationId={this.props.primaryLocationId}

          	employment_history_can={this.props.employment_history_can}

            setPropState={this.setPropState}
          />
          : null
          }

          {this.props.course_training_can.course_training_can_view == true ?
            <TrainedLocations 
              staffContactID={this.state.staffContactID}
              primaryLocationId={this.props.primaryLocationId}

        		  course_training_can={this.props.course_training_can}

              setPropState={this.setPropState}
            />
          : null
          }

        </div>
      );
   }
}

export default EmploymentInformation;
