/**
 * Signin Firebase
 */

import React, { Component } from 'react';
import { Helmet } from "react-helmet";

import moment from 'moment';
import Loader from '../../../Loader';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import SystemHelpers from '../../../Helpers/SystemHelper';

//table
import { MDBDataTable } from 'mdbreact';
import '@fortawesome/fontawesome-free/css/all.min.css';
import 'bootstrap-css-only/css/bootstrap.min.css';
import 'mdbreact/dist/css/mdb.css';

import CryptoAES from 'crypto-js/aes';
import CryptoENC from 'crypto-js/enc-utf8';
//table


class TrainedLocations extends Component {
  constructor(props) {
    super(props);

    this.state = {

        // Pagination 
        totalCount : 0,
        pageSize : 5,
        currentPage : 1,
        totalPages : 0,
        previousPage : false,
        nextPage : false,
        searchText : '',
        pagingData : {},
        TempsearchText:'',

        sortColumn : 'TrainedWork',
        SortType : false,
        IsSortingEnabled : true,
        // Pagination

        errormsg :  '',
        user_role: [],
        ListGrid : [],
        staffContactID:this.props.staffContactID,
        trainedWorkList: [],
        locationList:[],
        TraininType:[{"id": 'Yes',"name": "Yes"},
            {"id": 'No',"name": "No"}
        ],
        // Edit Model
        EdittrainedWork :  '',
        Editlocation :  '',
        EdittrainningType :  '',
        EditdateCompleted :  '',
        EditdateLastWork :  '',
        EditnoOfHoursWork :  '',
        EditID : '',
        trainedLocationId:'',
        // Edit Model

        // Add Model
        AddtrainedWork :  '',
        Addlocation :  '',
        AddtrainningType :  '',
        AdddateCompleted :  '',
        AdddateLastWork :  '',
        AddnoOfHoursWork :  '',
        // Add Model
        role_course_training_can: {},

        isDelete : false,

        header_data : [],
        staffContactFullname : localStorage.getItem('fullName')
    };
    this.setPropState = this.setPropState.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  setPropState(key, value) {
      this.setState({ [key]: value });
  }

  handleChange = input => e => {
    this.setState({ [input]: e.target.value.replace(/^\s+/g, '') });
    //console.log(input);
    if (this.state[input] != '') {
        delete this.state.errormsg[input];
    }
    
  }

  // Loader hide show method
  hideLoader = () => {
    this.setState({ loading: false });
  }

  showLoader = () => {
    this.setState({ loading: true });
  }
  // Loader hide show method

  

  componentDidMount() {
    this.setState({ Addlocation: this.props.primaryLocationId });
    console.log('didmount 123');
    console.log(this.props.primaryLocationId);
    /* Role Management */
     console.log('Role Store course_training_can');
     /*var getrole = SystemHelpers.GetRole();
     let course_training_can = getrole.course_training_can;
     this.setState({ role_course_training_can: course_training_can });
     console.log(course_training_can);*/

     console.log(this.props.course_training_can);
    let course_training_can = this.props.course_training_can;
    this.setState({ role_course_training_can: this.props.course_training_can });
    /* Role Management */

    // this.GetUserTrainedLocations();
    // this.GetProfile();

    // Delete Permison
    // Delete Permison
  }

  TabClickOnLoadTrainedLocations = () => e => {
    //debugger;
    e.preventDefault();

    this.GetUserTrainedLocations(this.state.currentPage,this.state.pageSize,this.state.searchText);
    this.GetProfile();
  }

  Edit_Update_Btn_Func(record){
    let return_push = [];

    if(this.state.role_course_training_can.course_training_can_update == true || this.state.role_course_training_can.course_training_can_delete == true){
      let Edit_push = [];
      if(this.state.role_course_training_can.course_training_can_update == true){
        Edit_push.push(
          <a href="#" onClick={this.EditRecord(record)} data-toggle="modal" data-target="#EmploymentInformation_trained_locations_Edit_modal" className="dropdown-item"><i className="fa fa-pencil m-r-5" /> Edit</a>
        );
      }
      let Delete_push = [];
      if(this.state.role_course_training_can.course_training_can_delete == true){
        if(record.isDelete == false)
        {
          Delete_push.push(
            <a href="#" onClick={this.EditRecord(record)} className="dropdown-item" data-toggle="modal" data-target="#delete_trained_locations"><i className="fa fa-trash-o m-r-5" /> Inactive</a>
          );
        }
        else
        {
          Delete_push.push(
            <a href="#" onClick={this.EditRecord(record)} className="dropdown-item" data-toggle="modal" data-target="#delete_trained_locations"><i className="fa fa-trash-o m-r-5" /> Active</a>
          );
        }
      }
      
      return_push.push(
        <div className="dropdown dropdown-action">
          <a aria-expanded="false" data-toggle="dropdown" className="action-icon dropdown-toggle" href="#"><i className="material-icons">more_vert</i></a>
          <div className="dropdown-menu dropdown-menu-right">
            {Edit_push}
            {Delete_push}
          </div>
        </div>
      );
    }
    return return_push;
  }

  EditRecord = (record) => e => {
    e.preventDefault();

    this.setState({ errormsg: '' });

    this.setState({EdittrainedWork :  record.trainedWork });
    this.setState({Editlocation :  record.locationId });
    this.setState({EdittrainningType :  record.trainningType });
    this.setState({EditdateCompleted :  moment(record.dateCompleted,process.env.API_DATE_FORMAT).format('YYYY-MM-DD')});
    this.setState({EditdateLastWork :  moment(record.dateLastWork,process.env.API_DATE_FORMAT).format('YYYY-MM-DD')});
    this.setState({EditnoOfHoursWork :  record.noOfHoursWork });
    this.setState({trainedLocationId :  record.trainedLocationId });

    this.setState({ isDelete: record.isDelete });
  }

  GetProfile(){
    // console.log(localStorage.getItem("token"));
    this.showLoader();
    var url=process.env.API_API_URL+'GetUserBasicInfoById?contactId='+this.state.staffContactID;
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
    }).then((response) => response.json())
    .then(data => {
        // console.log("responseJson GetUserBasicInfoById Trained Locations ");
        // console.log(data);
        if (data.responseType === "1") {
            
            this.setState({ Addlocation: data.data.locationIdGuid });
        }else{
              if(data.message == 'Authorization has been denied for this request.'){
                this.SessionOut();
              }else{
                this.ToastError(data.message);
              }
              
        }
        this.hideLoader();
        
    })
    .catch(error => {
      //this.props.history.push("/error-500");
    });
  }

  TableHeaderDesign()
  {
      if(this.state.role_course_training_can.course_training_can_delete == true || this.state.role_course_training_can.course_training_can_delete == "true")
      {
        let columns = [];
        columns.push(<tr>
            <th>Trained/Worked</th>
            <th>Location</th>
            <th>Training Type</th>
            <th>Date Completed</th>
            <th>Date Last Worked</th>
            <th># of Hrs Worked</th>
            <th>Status</th>
            <th>Action</th>
          </tr>
        );
        return columns;
      }
      else
      {
        let columns = [];
        columns.push(<tr>
            <th>Trained/Worked</th>
            <th>Location</th>
            <th>Training Type</th>
            <th>Date Completed</th>
            <th>Date Last Worked</th>
            <th># of Hrs Worked</th>
            <th>Action</th>
          </tr>
        );
        return columns; 
      }
  }

  GetUserTrainedLocations(currentPage,pageSize,searchText){

    /* Role Management */
    var getrole = SystemHelpers.GetRole();
    /* Role Management */

    let canDelete = getrole.course_training_can.course_training_can_delete;

    this.showLoader();

    this.setState({ ListGrid : [] });

    // Pagination
    let bodyarray = {};
    bodyarray["currentPage"] = 1;
    bodyarray["nextPage"] = false;
    bodyarray["pageSize"] = 5;
    bodyarray["previousPage"] = false;
    bodyarray["totalCount"] = 0;
    bodyarray["totalPages"] = 0;
    
    this.setState({ pagingData : bodyarray });

    this.setState({ currentPage: currentPage });
    this.setState({ pageSize: pageSize });

    var sort_Column = this.state.sortColumn;
    var Sort_Type = this.state.SortType;
    
    var IsSortingEnabled = true;

    var url_paging_para = '&pageNumber='+currentPage+'&pageSize='+pageSize+'&searchText='+searchText+'&sortColumn='+sort_Column+'&SortType='+Sort_Type+'&IsSortingEnabled='+IsSortingEnabled;
    // Pagination

    var url=process.env.API_API_URL+'GetUserTrainedLocations?contactId='+this.state.staffContactID+'&canDelete='+canDelete+url_paging_para;
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      //body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
        // console.log("responseJson GetUserTrainedLocations");
        // console.log(data);
        // console.log(data.data.userRole);
        // debugger;
        if (data.responseType === "1") {
            // Profile & Contact
            //this.setState({ ListGrid: data.data.userTrainedLocationView });
            this.setState({ header_data: this.TableHeaderDesign() });

            this.setState({ ListGrid: this.rowData(data.data.userTrainedLocationView) })
            this.setState({ trainedWorkList: data.data.trainedWorkList });
            this.setState({ locationList: data.data.trainedLocations });
            
            this.setState({ pagingData: data.pagingData });
        }else{
              if(data.message == 'Authorization has been denied for this request.'){
                this.SessionOut();

              }else{
                SystemHelpers.ToastError(data.message);
              }
              
        }
        this.hideLoader();
        
    })
    .catch(error => {
      this.props.history.push("/error-500");
    });
  }

  UpdateRecord = () => e => {
    //debugger;
    e.preventDefault();

    let step1Errors = {};
    
    //alert(this.state["EdittrainedWork"]);
    if (this.state["EdittrainedWork"] == 0 || this.state["EdittrainedWork"] == "" || this.state["EdittrainedWork"] == null) {
      step1Errors["EdittrainedWork"] = "Please Select Trained/Worked.";
    }

    if (this.state["Editlocation"] == "" || this.state["Editlocation"] == null) {
      step1Errors["Editlocation"] = "Location is mandatory.";
    }

    /*if (this.state["EdittrainningType"] == "" || this.state["EdittrainningType"] == null) {
      step1Errors["EdittrainningType"] = "Training Type is mandatory";
    }*/

    if (this.state["EditdateCompleted"] == ""  || this.state["EditdateCompleted"] == null) {
      step1Errors["EditdateCompleted"] = "Date Completed is mandatory";
    }

    if (this.state["EditdateLastWork"] == "" || this.state["EditdateLastWork"] == null) {
      step1Errors["EditdateLastWork"] = "Date Last Worked is mandatory";
    }

    if (this.state["EditnoOfHoursWork"] == "" || this.state["EditnoOfHoursWork"] == null) {
      step1Errors["EditnoOfHoursWork"] = "of Hrs Worked is mandatory";
    }

    

    console.log(step1Errors);

    this.setState({ errormsg: step1Errors });
        
    if (Object.keys(step1Errors).length > 0) {
        return false;
    }

    var EditdateCompleted=moment(this.state["EditdateCompleted"]).format('MM-DD-YYYY');
    //var NewEditdateCompleted = moment(EditdateCompleted, "MM-DD-YYYY").add(1, 'days');
    
    var EditdateLastWork=moment(this.state["EditdateLastWork"]).format('MM-DD-YYYY');
    //var NewEditdateLastWork = moment(EditdateLastWork, "MM-DD-YYYY").add(1, 'days');

    this.showLoader();

    let ArrayJson = {
          trainedWork: this.state["EdittrainedWork"],
          locationId: this.state["Editlocation"],
          trainningType: this.state["EdittrainningType"],
          dateCompleted: EditdateCompleted,
          dateLastWork: EditdateLastWork,
          noOfHoursWork: this.state["EditnoOfHoursWork"],
          trainedLocationId: this.state["trainedLocationId"]
    };
     
    let bodyarray = {};
    bodyarray["contactId"] = this.state.staffContactID;
    bodyarray["userTrainedLocation"] = ArrayJson;
    bodyarray["userName"] = this.state.staffContactFullname;

    console.log(bodyarray);
    //return false;
    var url=process.env.API_API_URL+'UpdateUserTrainedLocation';
    fetch(url, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
        console.log("responseJson UpdateUserTrainedLocation");
        console.log(data);
        //console.log(responseJson);
        // debugger;
        if (data.responseType === "1") {
            SystemHelpers.ToastSuccess(data.responseMessge);
            $( ".close" ).trigger( "click" );
            this.GetUserTrainedLocations(this.state.currentPage,this.state.pageSize,this.state.searchText);
            //this.props.history.push('/dashboard');   
        }
        else if (data.responseType === "2") {
            SystemHelpers.ToastError(data.responseMessge);
        }
        this.hideLoader();
        
    })
    .catch(error => {
      this.props.history.push("/error-500");
    });
  }

  ClearRecord = ()=> e => {
    e.preventDefault();

    this.setState({ AddtrainedWork: 0 });
    //this.setState({ Addlocation: 0 });
    this.setState({ AddtrainningType: 0 });
    this.setState({ AdddateCompleted: '' });
    this.setState({ AdddateLastWork: '' });
    this.setState({ AddnoOfHoursWork: '' });

    this.setState({ errormsg: '' });

    //this.GetProfile(); // this.props.primaryLocationId
    this.setState({ Addlocation: localStorage.getItem("primaryLocationGuid") });
    //this.setState({ Addlocation: localStorage.getItem("primaryLocationId") });
    //this.setState({ Addlocation: this.props.primaryLocationId });
  }

  AddRecord = () => e => {
    //debugger;
    e.preventDefault();

    let step1Errors = {};
    //alert(this.state["Addlocation"]);
    
    if (this.state["AddtrainedWork"] == "" || this.state["AddtrainedWork"] == null) {
      step1Errors["AddtrainedWork"] = "Please Select Trained/Worked.";
    }

    if (this.state["Addlocation"] == "" || this.state["Addlocation"] == null) {
      step1Errors["Addlocation"] = "Location is mandatory.";
    }

    /*if (this.state["AddtrainningType"] == "" || this.state["AddtrainningType"] == null) {
      step1Errors["AddtrainningType"] = "Training Type is mandatory";
    }*/

    if (this.state["AdddateCompleted"] == ""  || this.state["AdddateCompleted"] == null) {
      step1Errors["AdddateCompleted"] = "Date Completed is mandatory";
    }

    if (this.state["AdddateLastWork"] == "" || this.state["AdddateLastWork"] == null) {
      step1Errors["AdddateLastWork"] = "Date Last Worked is mandatory";
    }

    if (this.state["AddnoOfHoursWork"] == "" || this.state["AddnoOfHoursWork"] == null) {
      step1Errors["AddnoOfHoursWork"] = "of Hrs Worked is mandatory";
    }

    

    console.log(step1Errors);

    this.setState({ errormsg: step1Errors });
        
    if (Object.keys(step1Errors).length > 0) {
        return false;
    }

    //return false;
    this.showLoader();

    var AdddateCompleted=moment(this.state["AdddateCompleted"]).format('MM-DD-YYYY');
    //var NewAdddateCompleted = moment(AdddateCompleted, "MM-DD-YYYY").add(1, 'days');
    
    var AdddateLastWork=moment(this.state["AdddateLastWork"]).format('MM-DD-YYYY');
    //var NewAdddateLastWork = moment(AdddateLastWork, "MM-DD-YYYY").add(1, 'days');

    let ArrayJson = {
          trainedWork: this.state["AddtrainedWork"],
          locationId: this.state["Addlocation"],
          trainningType: this.state["AddtrainningType"],
          dateCompleted: AdddateCompleted,
          dateLastWork: AdddateLastWork,
          noOfHoursWork: this.state["AddnoOfHoursWork"]
    };
     
    let bodyarray = {};
    bodyarray["contactId"] = this.state.staffContactID;
    bodyarray["userTrainedLocation"] = ArrayJson;

    console.log(bodyarray);
    //return false;
    var url=process.env.API_API_URL+'CreateUserTrainedLocation';
    fetch(url, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
        console.log("responseJson CreateUserTrainedLocation");
        console.log(data);
        //console.log(responseJson);
        // debugger;
        if (data.responseType === "1") {

            this.setState({ AddtrainedWork: 0 });
            this.setState({ Addlocation: 0 });
            this.setState({ AddtrainningType: 0 });
            this.setState({ AdddateCompleted: '' });
            this.setState({ AdddateLastWork: '' });
            this.setState({ AddnoOfHoursWork: '' });

            SystemHelpers.ToastSuccess(data.responseMessge);
            $( ".close" ).trigger( "click" );
            this.GetUserTrainedLocations(this.state.currentPage,this.state.pageSize,this.state.searchText);
            //this.props.history.push('/dashboard');   
        }
        else if (data.responseType === "2") {
            SystemHelpers.ToastError(data.responseMessge);
        }
        this.hideLoader();
        
    })
    .catch(error => {
      this.props.history.push("/error-500");
    });
  }

  DeleteRecord = () => e => {
    e.preventDefault();

    var isdelete = '';
    if(this.state.isDelete== true)
    {
      isdelete = false;
    }
    else
    {
      isdelete = true;
    }

    this.showLoader();
    var url=process.env.API_API_URL+'DeleteUserTrainedLocation?trainedLocationId='+this.state.trainedLocationId+'&isDelete='+isdelete+'&userName='+this.state.staffContactFullname;
    fetch(url, {
      method: 'PUT',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      //body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
        console.log("responseJson DeleteUserTrainedLocation");
        console.log(data);
        //console.log(data.data.userRole);
        // debugger;
        if (data.responseType === "1") {
            // Profile & Contact
            SystemHelpers.ToastSuccess(data.responseMessge);
            this.GetUserTrainedLocations(this.state.currentPage,this.state.pageSize,this.state.searchText);
            $( ".cancel-btn" ).trigger( "click" );
            this.hideLoader();
        }else if (data.responseType == "2" || data.responseType == "3") {
            SystemHelpers.ToastError(data.responseMessge);
            $( ".cancel-btn" ).trigger( "click" );
            this.hideLoader();
        }else{
              if(data.message == 'Authorization has been denied for this request.'){
                this.SessionOut();

              }else{
                SystemHelpers.ToastError(data.message);
              }
              this.hideLoader();
              $( ".cancel-btn" ).trigger( "click" );
        }
        
        
    })
    .catch(error => {
      this.props.history.push("/error-500");
    });
  }

  rowData(ListGrid) {
    // console.log(ListGrid)

    /* Role Management */
    var getrole = SystemHelpers.GetRole();
    let canDelete = getrole.course_training_can.course_training_can_delete;
    /* Role Management */

    

      var ListGrid_length = ListGrid.length;
      let dataArray = [];
      var i=1;
      for (var z = 0; z < ListGrid_length; z++) {
        var tempdataArray = [];
        //tempdataArray.rownum = i;

        var trainningType = "";
        if(ListGrid[z].trainningType == 0){
          trainningType = "";
        }else{
          trainningType = ListGrid[z].trainningType;
        }

        var status = "";
        if(canDelete == true){
          if(ListGrid[z].isDelete == true){
            status = <div><span class="badge bg-inverse-warning">Inactive</span></div>;
          }else{
            status = <div><span class="badge bg-inverse-success">Active</span></div>;
          }
        }

        tempdataArray.push(<tr key={z}>
          <td>{ListGrid[z].trainedWork}</td>
          <td>{tempdataArray.location = ListGrid[z].location}</td>
          <td>{trainningType}</td>
          <td>{moment(ListGrid[z].dateCompleted,process.env.API_DATE_FORMAT).format(process.env.DATE_FORMAT)}</td>
          <td>{moment(ListGrid[z].dateLastWork,process.env.API_DATE_FORMAT).format(process.env.DATE_FORMAT)}</td>
          <td>{ListGrid[z].noOfHoursWork}</td>
          <td>{status}</td>
          <td>{this.Edit_Update_Btn_Func(ListGrid[z])}</td>
        </tr>);

        dataArray.push(tempdataArray);
        i++;
      }
      return dataArray;
  }

  // Pagination Design
  PaginationDesign ()
  {
    let PageOutput = [];
    // console.log('pagination');
    // console.log(this.state.pagingData);
    
    if(this.state.pagingData !="" && this.state.pagingData !="undefined")
    {
      var Page_Count = this.state.pagingData.totalPages;
      //alert(this.state.pagingData.currentPage);
      // console.log('page count = ' + Page_Count);
      /* pagination count */

      var Page_Start=1;
      var Page_End=1;

      if(this.state.pagingData.currentPage == 1){
        Page_Start=1;

        if(Page_Count <= 10){
          Page_End=Page_Count;
        }else{
          Page_End=10;
        }
      }else{
        if(this.state.pagingData.currentPage < 5){
          Page_Start=1;
          Page_End=Page_Count;
          //Page_End=parseInt(this.state.pagingData.currentPage) + parseInt(6);
          // console.log("Page_End 1 "+ Page_End);
        }else{
          Page_Start=parseInt(this.state.pagingData.currentPage) - parseInt(4);
          Page_End=parseInt(this.state.pagingData.currentPage) + parseInt(6);
          // console.log("Page_End 2 "+ Page_End);
          if(Page_End > Page_Count){
            Page_End=Page_Count;
            // console.log("Page_End 3 "+ Page_End);
          }
        }
      }

      let Page = [];
      var i = 1;
      for (var z=Page_Start; z <= Page_End ; z++)
      {
        if(z==this.state.pagingData.currentPage)
        {
          Page.push(<li className="page-item active pk-active">
            <a className="page-link pk-active" id={z} href="#" onClick={this.PageGetGridData}>{z}<span className="sr-only">(current)</span></a>
          </li>);
        }
        else
        {
          Page.push(<li className="page-item"><a className="page-link" id={z} href="#" onClick={this.PageGetGridData} >{z}</a></li>);
        }
        i++;
      }

      let PagePrev = [];

      if(this.state.pagingData.currentPage == 1){
        PagePrev.push(<li className="page-item disabled">
          <a className="page-link" href="#">Previous</a>
        </li>);
      }else{
        PagePrev.push(<li className="page-item">
          <a className="page-link" href="#" id={parseInt(this.state.pagingData.currentPage)-parseInt(1)} tabIndex={-1} onClick={this.PageGetGridData}>Previous</a>
        </li>);
      }

      let PageNext = [];

      if(this.state.pagingData.currentPage == Page_Count){
        PageNext.push(<li className="page-item disabled">
          <a className="page-link" href="#">Next</a>
        </li>);
      }else{
        PageNext.push(
          <li className="page-item">
            <a className="page-link" href="#" id={parseInt(this.state.pagingData.currentPage)+parseInt(1)} onClick={this.PageGetGridData}>Next</a>
          </li>
        );
      }

      let PageLast = [];

      if(this.state.pagingData.currentPage == Page_Count){
        PageLast.push(<li className="page-item disabled">
          <a className="page-link" href="#">Last</a>
        </li>);
      }else{
        PageLast.push(
          <li className="page-item">
            <a className="page-link" href="#" id={parseInt(Page_Count)} onClick={this.PageGetGridData}>Last</a>
          </li>
        );
      }
      
      PageOutput.push(<section className="comp-section" id="comp_pagination">
                        <div className="pagination-box">
                          <div>
                            <ul className="pagination">
                              
                              {PagePrev}
                              {Page}
                              {PageNext}
                              {PageLast}
                              
                            </ul>
                          </div>
                        </div>
                      </section>);
    }
    
    return PageOutput;
  }

  PageGetGridData = e => {

    e.preventDefault();
    let current_page= e.target.id;
    this.GetUserTrainedLocations(current_page,this.state.pageSize,this.state.searchText)
  }

  SearchGridData = e => {
    this.setState({ pageSize: this.state.TempsearchText });
    this.GetUserTrainedLocations(1,this.state.pageSize,this.state.TempsearchText);
  }
  // Pagination Design

  render() {
    
    const { EdittrainedWork, Editlocation, EdittrainningType, EditdateCompleted, EditdateLastWork, EditnoOfHoursWork } = this.props;
      return (
        <div>
        {/* Toast & Loder method use */}
            
        {(this.state.loading) ? <Loader /> : null} 
        {/* Toast & Loder method use */}
        <div className="row">
          <div className="col-md-12 d-flex">
            <div className="card profile-box flex-fill">
              <div className="row">
                <button className="btn btn-primary submit-btn pk-profiletab-refreshbtn-hide" id="TabClickOnLoadTrainedLocations" onClick={this.TabClickOnLoadTrainedLocations()}>Refresh</button>
              </div>
              <div className="card-body">
                {this.state.role_course_training_can.course_training_can_create == true ?
                  <h3 className="card-title">Trained Locations<a href="#" className="edit-icon" data-toggle="modal" data-target="#EmploymentInformation_trained_locations_Add_modal"><i className="fa fa-plus" /></a></h3>
                  : <h3 className="card-title">Trained Locations <a href="#" className="phss-lock"><i className="fa fa-lock" /></a></h3>
                }

                {/* Page Per Record and serach design*/}
                  <div className="row filter-row">
                    <div className="col-sm-6 col-md-2"> 
                      <div className="form-group form-focus select-focus">
                        <select className="form-control floating" value={this.state.pageSize}  onChange={this.handleChange('pageSize')}> 
                          <option value="5">5/Page</option>
                          <option value="10">10/Page</option>
                          <option value="50">50/Page</option>
                          <option value="100">100/Page</option>
                        </select>
                        <label className="focus-label">Per Page</label>
                      </div>
                    </div>
                    
                    <div className="col-sm-6 col-md-3">
                      <div className="form-group form-focus focused">
                        <label className="focus-label">Sorting</label>
                        <select className="form-control floating" id="sortColumn" value={this.state.sortColumn} onChange={this.handleChange('sortColumn')}> 
                          {/*<option value="">-</option>*/}
                          <option value="TrainedWork">Trained/Worked</option>
                          <option value="Location">Location</option>
                          <option value="TrainningType">Training Type</option>
                          <option value="DateCompleted">Date Completed</option>
                          <option value="DateLastWork">Date Last Worked</option>
                        </select>
                      </div>
                    </div> 

                    <div className="col-sm-6 col-md-2">
                      <div className="form-group form-focus focused">
                        <label className="focus-label">Sorting Order</label>
                        <select className="form-control floating" id="SortTypeId" value={this.state.SortType} onChange={this.handleChange('SortType')}> 
                          {/*<option value="">-</option>*/}
                          <option value="false">Ascending</option>
                          <option value="true">Descending</option>
                        </select>
                      </div>
                    </div> 

                    <div className="col-sm-6 col-md-3">  
                      <div className="form-group form-focus focused">
                        <input className="form-control floating" type="text" value={this.state.TempsearchText}  onChange={this.handleChange('TempsearchText')} placeholder="Search by Location"  />
                        <label className="focus-label">Search</label>
                      </div>
                    </div>

                    <div className="col-sm-6 col-md-2">  
                      <a href="#" className="btn btn-success btn-block" onClick={this.SearchGridData}> Search </a>  
                    </div> 
                  </div>
                {/* Page Per Record and serach design*/}
                
                <div className="row">
                  <div className="col-md-12">
                    <div className="table-responsive">
                      
                      <table className="table table-striped custom-table mb-0 datatable">
                        <thead>
                          { this.state.ListGrid.length > 0 ? this.state.header_data : null}
                        </thead>
                        <tbody>
                          
                          {this.state.ListGrid.length > 0 ? this.state.ListGrid : null}
                        </tbody>
                       </table> 

                      {this.PaginationDesign()}

                    </div>
                  </div>
                </div>

              </div>
            </div>
          </div>
        </div>
        {/* /********** Employment Information Tab Modals *********** */}
        {/* Trained Locations Modal */}
            <div id="EmploymentInformation_trained_locations_Add_modal" className="modal custom-modal fade" role="dialog">
              <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div className="modal-content">
                  <div className="modal-header">
                    <h5 className="modal-title">Trained Locations</h5>
                    <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={this.ClearRecord()} >
                      <span aria-hidden="true">×</span>
                    </button>
                  </div>
                  <div className="modal-body">
                    <form>
                      <div className="card">
                        <div className="card-body">
                          <div className="row">
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Trained/Worked<span className="text-danger">*</span></label>
                                <select className="form-control" value={this.state.AddtrainedWork}  onChange={this.handleChange('AddtrainedWork')}>
                                  <option value=''>-</option>
                                  {this.state.trainedWorkList.map(( listValue, index ) => {
                                    return (
                                      <option key={index} value={listValue.name}>{listValue.name}</option>
                                    );
                                  })}
                                </select>
                                <span className="form-text error-font-color">{this.state.errormsg["AddtrainedWork"]}</span>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Location<span className="text-danger">*</span></label>
                                <select className="form-control" value={this.state.Addlocation} onChange={this.handleChange('Addlocation')}>
                                  <option value="">-</option>
                                  {this.state.locationList.map(( listValue, index ) => {
                                    return (
                                      <option key={index} value={listValue.id}>{listValue.name}</option>
                                    );
                                  })}
                                </select>
                                <span className="form-text error-font-color">{this.state.errormsg["Addlocation"]}</span>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Training Type</label>
                                <select className="form-control" value={this.state.AddtrainningType} onChange={this.handleChange('AddtrainningType')}>
                                  <option value="">-</option>
                                  {this.state.TraininType.map(( listValue, index ) => {
                                    return (
                                      <option key={index} value={listValue.id}>{listValue.name}</option>
                                    );
                                  })}
                                </select>
                                <span className="form-text error-font-color">{this.state.errormsg["AddtrainningType"]}</span>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Date Completed<span className="text-danger">*</span></label>
                                  <input className="form-control" type="date"  value={this.state.AdddateCompleted} onChange={this.handleChange('AdddateCompleted')} />
                                  <span className="form-text error-font-color">{this.state.errormsg["AdddateCompleted"]}</span>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Date Last Worked<span className="text-danger">*</span></label>
                                  <input className="form-control" type="date"  value={this.state.AdddateLastWork} onChange={this.handleChange('AdddateLastWork')} />
                                  <span className="form-text error-font-color">{this.state.errormsg["AdddateLastWork"]}</span>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label># of Hrs Worked<span className="text-danger">*</span></label>
                                <input type="number" className="form-control" value={this.state.AddnoOfHoursWork} onChange={this.handleChange('AddnoOfHoursWork')} />
                                <span className="form-text error-font-color">{this.state.errormsg["AddnoOfHoursWork"]}</span>
                              </div>
                            </div>
                          </div>
                          <div className="submit-section">
                            <button className="btn btn-primary submit-btn" onClick={this.AddRecord()}>Submit</button>
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
        {/* //Trained Locations Modal */}
        {/* Trained Locations Edit Modal */}
            <div id="EmploymentInformation_trained_locations_Edit_modal" className="modal custom-modal fade" role="dialog">
              <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div className="modal-content">
                  <div className="modal-header">
                    <h5 className="modal-title">Trained Locations</h5>
                    <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">×</span>
                    </button>
                  </div>
                  <div className="modal-body">
                    <form>
                      <div className="card">
                        <div className="card-body">
                          <div className="row">
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Trained/Worked<span className="text-danger">*</span></label>
                                <select className="form-control" value={this.state.EdittrainedWork}  onChange={this.handleChange('EdittrainedWork')}>
                                  <option value='0'>-</option>
                                  {this.state.trainedWorkList.map(( listValue, index ) => {
                                    return (
                                      <option key={index} value={listValue.name}>{listValue.name}</option>
                                    );
                                  })}
                                </select>
                                <span className="form-text error-font-color">{this.state.errormsg["EdittrainedWork"]}</span>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Location<span className="text-danger">*</span></label>
                                <select className="form-control" value={this.state.Editlocation} onChange={this.handleChange('Editlocation')}>
                                  <option value="">-</option>
                                  {this.state.locationList.map(( listValue, index ) => {
                                    return (
                                      <option key={index} value={listValue.id}>{listValue.name}</option>
                                    );
                                  })}
                                </select>
                                <span className="form-text error-font-color">{this.state.errormsg["Editlocation"]}</span>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Training Type</label>
                                <select className="form-control" value={this.state.EdittrainningType} onChange={this.handleChange('EdittrainningType')}>
                                  <option value="">-</option>
                                  {this.state.TraininType.map(( listValue, index ) => {
                                    return (
                                      <option key={index} value={listValue.id}>{listValue.name}</option>
                                    );
                                  })}
                                </select>
                                <span className="form-text error-font-color">{this.state.errormsg["EdittrainningType"]}</span>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Date Completed<span className="text-danger">*</span></label>
                                  <input className="form-control" type="date"  value={this.state.EditdateCompleted} onChange={this.handleChange('EditdateCompleted')} />
                                  <span className="form-text error-font-color">{this.state.errormsg["EditdateCompleted"]}</span>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Date Last Worked<span className="text-danger">*</span></label>
                                <input className="form-control" type="date"  value={this.state.EditdateLastWork} onChange={this.handleChange('EditdateLastWork')} />
                                <span className="form-text error-font-color">{this.state.errormsg["EditdateLastWork"]}</span>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label># of Hrs Worked<span className="text-danger">*</span></label>
                                <input type="number" className="form-control" value={this.state.EditnoOfHoursWork} onChange={this.handleChange('EditnoOfHoursWork')} />
                                <span className="form-text error-font-color">{this.state.errormsg["EditnoOfHoursWork"]}</span>
                              </div>
                            </div>
                          </div>
                          <div className="submit-section">
                            <button className="btn btn-primary submit-btn" onClick={this.UpdateRecord()}>Update</button>
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
        {/* //Trained Locations Edit Modal */}

        {/* Delete Trained Locations  Modal */}
            <div className="modal custom-modal fade" id="delete_trained_locations" role="dialog">
              <div className="modal-dialog modal-dialog-centered">
                <div className="modal-content">
                  <div className="modal-body">
                    <div className="form-header">
                      <h3>Trained Locations</h3>
                      <p>Are you sure you want to mark trained locations as {this.state.isDelete == true ? 'Active' : 'Inactive' } ?</p>
                    </div>
                    <div className="modal-btn delete-action">
                      <div className="row">
                        <div className="col-6">
                          <a  onClick={this.DeleteRecord()} className="btn btn-primary continue-btn">{this.state.isDelete == true ? 'Active' : 'Inactive' }</a>
                        </div>
                        <div className="col-6">
                          <a href="" data-dismiss="modal" className="btn btn-primary cancel-btn">Cancel</a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        {/* /Delete Trained Locations Modal */}
        {/* /********** Employment Information Tab Modals *********** */}
        </div>
      );
   }
}

export default TrainedLocations;
