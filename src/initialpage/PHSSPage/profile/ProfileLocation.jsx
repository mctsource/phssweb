/**
 * Signin Firebase
 */

import React, { Component } from 'react';
import { Helmet } from "react-helmet";

import moment from 'moment';

import Loader from '../../Loader';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import SystemHelpers from '../../Helpers/SystemHelper';

//table
import { MDBDataTable } from 'mdbreact';
import '@fortawesome/fontawesome-free/css/all.min.css';
import 'bootstrap-css-only/css/bootstrap.min.css';
import 'mdbreact/dist/css/mdb.css';
//table

import CryptoAES from 'crypto-js/aes';
import CryptoENC from 'crypto-js/enc-utf8';

import Datetime from "react-datetime";

class ProfileLocation extends Component {
  constructor(props) {
    super(props);

    this.state = {

        // Pagination 
        totalCount : 0,
        pageSize : 5,
        currentPage : 1,
        totalPages : 0,
        previousPage : false,
        nextPage : false,
        searchText : '',
        pagingData : {},
        TempsearchText:'',

        sortColumn : 'IsPrimaryLocation',
        SortType : true,
        IsSortingEnabled : true,
        // Pagination

        ListGrid:[],
        errormsg : '',
        user_role: [],
        staffContactID:this.props.staffContactID,

        AllLocationName:[],
        
        AddlocationName:'',
        AddStartDate:'',
        AddEndDate:'',

        EditlocationName:'',
        EditStartDate:'',
        EditEndDate:'',
        EditId : '',

        role_locations_departments_can: {},

        isDelete : false,

        header_data : [],

        staffContactFullname : localStorage.getItem('fullName'),

        // Inactive 
        InactiveStartDate:'',
        InactiveEndDate:'',

        IsShowInactiveStartDate : false,
        IsShowInactiveEndDate : false,
        // Inactive 

        LocationListGrid:[]

    };
    this.setPropState = this.setPropState.bind(this);

    this.handleAddStartDate = this.handleAddStartDate.bind(this);
    this.handleAddEndDate = this.handleAddEndDate.bind(this);

    this.handleEditStartDate = this.handleEditStartDate.bind(this);
    this.handleEditEndDate = this.handleEditEndDate.bind(this);

    this.handleInactiveStartDate = this.handleInactiveStartDate.bind(this);
    this.handleInactiveEndDate = this.handleInactiveEndDate.bind(this);
  }

  handleAddStartDate = (date) =>{
    // console.log('AddStartDate => '+ date);
    this.setState({ AddStartDate : date });
    this.setState({ AddEndDate : '' });
  };

  handleAddEndDate = (date) =>{
    // console.log('AddEndDate => '+ date);
    this.setState({ AddEndDate : date });
  };

  handleEditStartDate = (date) =>{
    // console.log('EditStartDate => '+ date);
    this.setState({ EditStartDate : date });
    this.setState({ EditEndDate : '' });
  };

  handleEditEndDate = (date) =>{
    // console.log('EditEndDate => '+ date);
    this.setState({ EditEndDate : date });
  };

  handleInactiveStartDate = (date) =>{
    // console.log('InactiveStartDate => '+ date);
    this.setState({ InactiveStartDate : date });
    this.setState({ InactiveEndDate : '' });
  };

  handleInactiveEndDate = (date) =>{
    // console.log('InactiveEndDate => '+ date);
    this.setState({ InactiveEndDate : date });
  };

  validationAddEndDate = (currentDate) => {
    return currentDate.isAfter(moment(this.state.AddStartDate));
  };

  validationEditEndDate = (currentDate) => {
    return currentDate.isAfter(moment(this.state.EditStartDate));
  };

  validationInactiveEndDate = (currentDate) => {
    return currentDate.isAfter(moment(this.state.InactiveStartDate));
  };

  setPropState(key, value) {
      this.setState({ [key]: value });
  }

  handleChange = input => e => {
    this.setState({ [input]: e.target.value.replace(/^\s+/g, '') });
    // console.log(input);
    if (this.state[input] != '') {
        delete this.state.errormsg[input];
    }

    // if([input]=="AddStartDate")
    // {
    //   //this.setState({ AddEndDate: moment(e.target.value).format('YYYY-MM-DD') });
    //   this.setState({ AddEndDate: '' });
    // }

    // if([input]=="EditStartDate")
    // {
    //   //this.setState({ EditEndDate: moment(e.target.value).format('YYYY-MM-DD') });
    //   this.setState({ EditEndDate: '' });
    // }
  }

  // Loader hide show method
  hideLoader = () => {
    this.setState({ loading: false });
  }

  showLoader = () => {
    this.setState({ loading: true });
  }
  // Loader hide show method

  componentDidMount() {

    /* Role Management */
     // console.log('Role Store locations_departments_can');
     /*var getrole = SystemHelpers.GetRole();
     let locations_departments_can = getrole.locations_departments_can;
     this.setState({ role_locations_departments_can: locations_departments_can });
     // console.log(locations_departments_can);*/

    // console.log(this.props.locations_departments_can);
    let locations_departments_can = this.props.locations_departments_can;
    this.setState({ role_locations_departments_can: this.props.locations_departments_can });
    /* Role Management */

    //this.GetUsersByLocations();

    // Delete Permison
    // Delete Permison
    
  }

  TabClickOnLoadLocationsDepartments = () => e => {
    //debugger;
    e.preventDefault();

    this.GetUsersByLocations(this.state.currentPage,this.state.pageSize,this.state.searchText);
  }

  TableHeaderDesign()
  {
      if(this.state.role_locations_departments_can.locations_departments_can_delete == true || this.state.role_locations_departments_can.locations_departments_can_delete == "true")
      {
        let columns = [];
        columns.push(<tr>
            <th>Location Name</th>
            <th>Primary Location</th>
            <th>Start Date</th>
            <th>End Date</th>
            <th>Status</th>
            <th>Action</th>
          </tr>
        );
        return columns;
      }
      else
      {
        let columns = [];
        columns.push(<tr>
            <th>Location Name</th>
            <th>Primary Location</th>
            <th>Start Date</th>
            <th>End Date</th>
            <th>Action</th>
          </tr>
        );
        return columns; 
      }
  }

  GetUsersByLocations(currentPage,pageSize,searchText){
    this.showLoader();

    /* Role Management */
    var getrole = SystemHelpers.GetRole();
    let canDelete = getrole.locations_departments_can.locations_departments_can_delete;
    /* Role Management */

    this.setState({ ListGrid : [] });

    // Pagination
    let bodyarray = {};
    bodyarray["currentPage"] = 1;
    bodyarray["nextPage"] = false;
    bodyarray["pageSize"] = 5;
    bodyarray["previousPage"] = false;
    bodyarray["totalCount"] = 0;
    bodyarray["totalPages"] = 0;
    
    this.setState({ pagingData : bodyarray });

    this.setState({ currentPage: currentPage });
    this.setState({ pageSize: pageSize });

    var sort_Column = this.state.sortColumn;
    var Sort_Type = this.state.SortType;
    
    var IsSortingEnabled = true;

    var url_paging_para = '&pageNumber='+currentPage+'&pageSize='+pageSize+'&searchText='+searchText+'&sortColumn='+sort_Column+'&SortType='+Sort_Type+'&IsSortingEnabled='+IsSortingEnabled;
    // Pagination

    var url=process.env.API_API_URL+'GetUserProfileLocations?contactId='+this.state.staffContactID+'&canDelete='+canDelete+url_paging_para;
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      //body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
       console.log("responseJson GetUserProfileLocations");
       console.log(data);
      if (data.responseType === "1") {
        this.setState({ header_data: this.TableHeaderDesign() });

        this.setState({ ListGrid: this.rowData(data.data.locationsView) });
        this.setState({ LocationListGrid: data.data.locationsView });
        this.setState({ AllLocationName: data.data.locations });

        this.setState({ pagingData: data.pagingData });
      }else{
        if(data.message == 'Authorization has been denied for this request.'){
          SystemHelpers.SessionOut();
          this.props.history.push("/login");
        }else{
          SystemHelpers.ToastError(data.message);
        }
      }
      this.hideLoader();
    })
    .catch(error => {
      console.log('GetUserProfileLocations error');
      console.log(error);
      this.props.history.push("/error-500");
    });
  }

  rowData(ListGrid) {
    // console.log('RowData get Location department profile');

    /* Role Management */
    var getrole = SystemHelpers.GetRole();
    let canDelete = getrole.locations_departments_can.locations_departments_can_delete;
    /* Role Management */
    
    var ListGrid_length = ListGrid.length;
    let dataArray = [];
    var i=1;
    for (var z = 0; z < ListGrid_length; z++) {
      var tempdataArray = []; 

      var primaryLocation="";
      if(ListGrid[z].isPrimaryLocation == true || ListGrid[z].isPrimaryLocation == "true"){
        primaryLocation = "Yes";
      }else{
        primaryLocation = "No";
      }

      var startDate="";
      if(ListGrid[z].startDate == ""){
        startDate = "";
      }else{
        startDate = moment(ListGrid[z].startDate,process.env.API_DATE_FORMAT).format(process.env.DATE_FORMAT);
      }

      var endDate="";
      if(ListGrid[z].endDate == ""){
        endDate = "";
      }else{
        endDate = moment(ListGrid[z].endDate,process.env.API_DATE_FORMAT).format(process.env.DATE_FORMAT);
      }

      var status = "";
      if(canDelete == true)
      {
        if(ListGrid[z].isDelete == true){
          status = <div><span class="badge bg-inverse-warning">Inactive</span></div>;
        }else{
          status = <div><span class="badge bg-inverse-success">Active</span></div>;
        }
      }

      tempdataArray.push(<tr key={z}>
        <td>{ListGrid[z].locationName}</td>
        <td>{primaryLocation}</td>
        <td>{startDate}</td>
        <td>{endDate}</td>
        <td>{status}</td>
        <td>{this.Edit_Update_Btn_Func(ListGrid[z])}</td>
      </tr>); 

      dataArray.push(tempdataArray);
      i++; 
    }
    return dataArray;
  }

  Edit_Update_Btn_Func(record){
    let return_push = [];

    if(this.state.role_locations_departments_can.locations_departments_can_update == true || this.state.role_locations_departments_can.locations_departments_can_delete == true){
      let Edit_push = [];
      if(this.state.role_locations_departments_can.locations_departments_can_update == true){
        Edit_push.push(
          <a href="#" onClick={this.EditRecord(record)} className="dropdown-item" data-toggle="modal" data-target="#LocationsDepartments_Edit_modal"><i className="fa fa-pencil m-r-5" ></i> Edit</a>
        );
      }
      let Delete_push = [];
      if(this.state.role_locations_departments_can.locations_departments_can_delete == true){
        if(record.isDelete == false)
        {  
          if(record.isPrimaryLocation == false || record.isPrimaryLocation == "false"){
            Delete_push.push(
              <a href="#" onClick={this.DeleteInfo(record)}  className="dropdown-item" data-toggle="modal" data-target="#LocationsDepartments_Delete_modal"><i className="fa fa-trash-o m-r-5" ></i> Inactive</a>
            );
          }
        }
        else
        {
          Delete_push.push(
            <a href="#" onClick={this.DeleteInfo(record)}  className="dropdown-item" data-toggle="modal" data-target="#LocationsDepartments_Delete_modal"><i className="fa fa-trash-o m-r-5" ></i> Active</a>
          );
        }
      }
      
      return_push.push(
        <div className="dropdown dropdown-action">
          <a aria-expanded="false" data-toggle="dropdown" className="action-icon dropdown-toggle" href="#"><i className="material-icons">more_vert</i></a>
          <div className="dropdown-menu dropdown-menu-right">
            {Edit_push}
            {Delete_push}
          </div>
        </div>
      );
    }
    return return_push;
  }

  EditRecord = (record) => e => {
    e.preventDefault();
    // console.log('edit location profile');
    // console.log(record);

    this.setState({ errormsg: '' });

    this.setState({ EditId : record.id });
    this.setState({ locationId: record.locationId });
    this.setState({ EditlocationName: record.locationId });

    if(record.startDate !=""){
      //this.setState({ EditStartDate: moment(record.startDate).format('YYYY-MM-DD') });
      this.setState({ EditStartDate: moment(record.startDate,process.env.API_DATE_FORMAT) });
    }else{
      this.setState({ EditStartDate: "" });
    }
    
    if(record.endDate != ''){
      //this.setState({ EditEndDate: moment(record.endDate).format('YYYY-MM-DD') });
      this.setState({ EditEndDate: moment(record.endDate,process.env.API_DATE_FORMAT) });
    }
    else{
      this.setState({ EditEndDate: '' });
    }
  }

  DeleteInfo = (record) => e =>{
    e.preventDefault();

    this.setState({ locationId: record.id });
    this.setState({ isDelete: record.isDelete });

    if(record.startDate !=""){
      this.setState({ InactiveStartDate: moment(record.startDate,process.env.API_DATE_FORMAT) });
      this.setState({ IsShowInactiveStartDate : false });
    }else{
      this.setState({ InactiveStartDate: "" });
      this.setState({ IsShowInactiveStartDate : true });
    }
    
    if(record.endDate != ''){
      this.setState({ InactiveEndDate: moment(record.endDate,process.env.API_DATE_FORMAT) });
      this.setState({ IsShowInactiveEndDate : false });
    }
    else{
      this.setState({ InactiveEndDate: '' });
      this.setState({ IsShowInactiveEndDate : true });
    }

  }

  AddRecord = () => e => {
    //debugger;
    e.preventDefault();

    let step1Errors = {};
    
    if (this.state["AddlocationName"] == '') {
      step1Errors["AddlocationName"] = "Location Name is mandatory";
    }

    if (this.state["AddStartDate"] == '') {
      step1Errors["AddStartDate"] = "Start Date is mandatory";
    }

   

    if(this.state["AddEndDate"].length){
      SystemHelpers.ToastWarning('please select a valid date.');
      return false;
    }



    /* ********************** Location Validation ********************** */
    var ListGrid=this.state.LocationListGrid;
    var ListGrid_length = ListGrid.length;

    var CKAddStartDate = moment(this.state["AddStartDate"]).format('YYYY-MM-DD');
    var CKAddEndDate = moment(this.state["AddEndDate"]).format('YYYY-MM-DD');
    
    for (var z = 0; z < ListGrid_length; z++) {
      // ListGrid[z].AllLocationName  endDate startDate locationId id
      if(ListGrid[z].locationId  == this.state["AddlocationName"]){

        var CKLooPAddStartDate = moment(ListGrid[z].startDate,"MM-DD-YYYY").format('YYYY-MM-DD');
        var CKLooPAddEndDate = moment(ListGrid[z].endDate,"MM-DD-YYYY").format('YYYY-MM-DD');

        /*
        console.log(" ******************************************** ");

        console.log(CKLooPAddStartDate);
        console.log(CKLooPAddEndDate);
        console.log(CKAddStartDate);
        console.log(CKAddEndDate);

        var aa = moment(CKAddStartDate,"YYYY-MM-DD").diff(moment(CKLooPAddStartDate,"YYYY-MM-DD"),'days');
        console.log(aa);
        

        console.log(" ******************************************** ");
        */

        if(ListGrid[z].startDate == "" || ListGrid[z].endDate == ""){
          //SystemHelpers.ToastError("Location assignment already exists for same date range.");
          SystemHelpers.ToastWarning("Location already exists...!");
          return false;
        }


        var check_is_exists_start = moment(CKAddStartDate).isBetween(CKLooPAddStartDate, CKLooPAddEndDate, null, '[]');
        var check_is_exists_end = moment(CKAddEndDate).isBetween(CKLooPAddStartDate, CKLooPAddEndDate, null, '[]');
        if(check_is_exists_start == true){
          //SystemHelpers.ToastError("Location assignment already exists for same date range.");
          SystemHelpers.ToastWarning("Location already exists...!");
          return false;
        }
        if(check_is_exists_end == true){
          //SystemHelpers.ToastError("Location assignment already exists for same date range.");
          SystemHelpers.ToastWarning("Location already exists...!");
          return false;
        }


        var check_is_exists_start_small = moment(CKLooPAddStartDate).isBetween(CKAddStartDate, CKAddEndDate, null, '[]');
        var check_is_exists_end_small = moment(CKLooPAddEndDate).isBetween(CKAddStartDate, CKAddStartDate, null, '[]');
        if(check_is_exists_start == true){
          //SystemHelpers.ToastError("Location assignment already exists for same date range.");
          SystemHelpers.ToastWarning("Location already exists...!");
          return false;
        }
        if(check_is_exists_end == true){
          //SystemHelpers.ToastError("Location assignment already exists for same date range.");
          SystemHelpers.ToastWarning("Location already exists...!");
          return false;
        }

      }
    }

    
    //return false;
    /* ********************** Location Validation ********************** */

    this.setState({ errormsg: step1Errors });
        
    if (Object.keys(step1Errors).length > 0) {
        return false;
    }

    this.showLoader();
    var AddStartDate=moment(this.state["AddStartDate"]).format('MM-DD-YYYY');
    //var NewAddStartDate = moment(AddStartDate, "MM-DD-YYYY").add(1, 'days');

    var add_enddate ='';
    if (this.state["AddEndDate"] != '') {
      var add_enddate=moment(this.state["AddEndDate"]).format('MM-DD-YYYY');
      //var NewAddEndDate = moment(AddEndDate, "MM-DD-YYYY").add(1, 'days');
      //var add_enddate=moment(NewAddEndDate).format('MM-DD-YYYY');
    }    
    
    let ArrayJson = {
      //LocationAddress: this.state["AddlocationName"],
      locationId: this.state["AddlocationName"],
      //startDate: moment(NewAddStartDate).format('MM-DD-YYYY'),
      startDate: AddStartDate,
      endDate: add_enddate,
      id : null
    };
     
    let bodyarray = {};
    bodyarray["contactId"] = this.state.staffContactID;
    bodyarray["locationView"] = ArrayJson;

    // console.log(bodyarray);
    //return false;
    var url=process.env.API_API_URL+'CreateUpdateUserProfileLocation';
    fetch(url, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
      // console.log("responseJson CreateUpdateUserProfileLocation");
      // console.log(data);
      // debugger;
      if (data.responseType === "1") {
        //this.props.history.push('/dashboard');
        this.setState({ AddlocationName : '' });
        this.setState({ AddStartDate: '' });
        this.setState({ AddEndDate: '' });
        
        SystemHelpers.ToastSuccess(data.responseMessge);
        $( ".close" ).trigger( "click" );
        this.GetUsersByLocations(this.state.currentPage,this.state.pageSize,this.state.searchText);    
      }else if (data.responseType === "2") {
        this.setState({ AddlocationName : '' });
        this.setState({ AddStartDate: '' });
        this.setState({ AddEndDate: '' });
        
        SystemHelpers.ToastWarning(data.responseMessge);
        $( ".close" ).trigger( "click" );
        this.GetUsersByLocations(this.state.currentPage,this.state.pageSize,this.state.searchText);    
      }else if (data.responseType === "3") {
        this.setState({ AddlocationName : '' });
        this.setState({ AddStartDate: '' });
        this.setState({ AddEndDate: '' });
        
        SystemHelpers.ToastWarning("Location already exists...!");
        $( ".close" ).trigger( "click" );
        this.GetUsersByLocations(this.state.currentPage,this.state.pageSize,this.state.searchText);    
      }
      else{
        SystemHelpers.ToastError(data.message  );
      }
      this.hideLoader();
    })
    .catch(error => {
      this.props.history.push("/error-500");
    });
    return false;
  }

  ClearRecord = ()=> e => {
    e.preventDefault();

    this.setState({ AddlocationName : '' });
    this.setState({ AddStartDate: '' });
    this.setState({ AddEndDate: '' });

    this.setState({ errormsg: '' });

    this.setState({ IsShowInactiveStartDate: false });
    this.setState({ IsShowInactiveEndDate: false });
  }

  UpdateRecord = () => e => {
    //debugger;
    e.preventDefault();

    let step1Errors = {};
    
    if (this.state["EditlocationName"] == '') {
      step1Errors["EditlocationName"] = "Location Name is mandatory";
    }

    if (this.state["EditStartDate"] == '') {
      step1Errors["EditStartDate"] = "Start Date is mandatory";
    }

    if(this.state["EditEndDate"].length){
      SystemHelpers.ToastWarning('please select a valid date.');
      return false;
    }

    this.setState({ errormsg: step1Errors });
        
    if (Object.keys(step1Errors).length > 0) {
      return false;
    }

    this.showLoader();
  
    var EditStartDate=moment(this.state["EditStartDate"]).format('MM-DD-YYYY');
    //var NewEditStartDate = moment(EditStartDate, "MM-DD-YYYY").add(1, 'days');
    
    var edit_end_date ='';
    if (this.state["EditEndDate"] != '') {
      var edit_end_date=moment(this.state["EditEndDate"]).format('MM-DD-YYYY');
      //var NewEditEndDate = moment(EditEndDate, "MM-DD-YYYY").add(1, 'days');
      //var edit_end_date=moment(NewEditEndDate).format('MM-DD-YYYY');
    }

    let ArrayJson = {
        //locationName: this.state["EditlocationName"],
        //startDate: moment(NewEditStartDate).format('MM-DD-YYYY'),
        startDate: EditStartDate,
        endDate: edit_end_date,
        locationId: this.state["EditlocationName"],
        id: this.state.EditId
    };
     
    let bodyarray = {};
    bodyarray["contactId"] = this.state.staffContactID;
    bodyarray["locationView"] = ArrayJson;
    bodyarray["userName"] = this.state.staffContactFullname;

    // console.log(bodyarray);
    //return false;
    var url=process.env.API_API_URL+'CreateUpdateUserProfileLocation';
    fetch(url, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
      // console.log("responseJson CreateUpdateUserProfileLocation");
      // console.log(data);
      // debugger;
      if (data.responseType === "1") {
        SystemHelpers.ToastSuccess(data.responseMessge);
        $( ".close" ).trigger( "click" );
        this.GetUsersByLocations(this.state.currentPage,this.state.pageSize,this.state.searchText);    
      }
      else{
        SystemHelpers.ToastError(data.responseMessge  );
      }
      this.hideLoader();
    })
    .catch(error => {
      console.log('CreateUpdateUserProfileLocation error');
      console.log(error);
      this.props.history.push("/error-500");
    });
    return false;
  }

  DeleteRecord = () => e => {
    e.preventDefault();

    let step1Errors = {};

    if(this.state.IsShowInactiveStartDate == true){
      if (this.state["InactiveStartDate"] == '') {
        step1Errors["InactiveStartDate"] = "Start Date is mandatory";
      }
    }
      
    if(this.state.IsShowInactiveEndDate == true){
      if (this.state["InactiveEndDate"] == '') {
        step1Errors["InactiveEndDate"] = "Start Date is mandatory";
      }
    }
      

    this.setState({ errormsg: step1Errors });
        
    if (Object.keys(step1Errors).length > 0) {
        return false;
    }

    var isdelete = '';
    if(this.state.isDelete== true)
    {
      isdelete = false;
    }
    else
    {
      isdelete = true;
    }

    this.showLoader();
    // console.log(this.state.locationId);

    var InactiveStartDate = "";
    if(this.state.IsShowInactiveStartDate == true){
      InactiveStartDate=moment(this.state["InactiveStartDate"]).format('MM-DD-YYYY');
    }
    
    var InactiveEndDate = "";
    if(this.state.IsShowInactiveEndDate == true){
      InactiveEndDate=moment(this.state["InactiveEndDate"]).format('MM-DD-YYYY');
    }
    

    var url=process.env.API_API_URL+'DeleteUserLocations?locationId='+this.state.locationId+'&isDelete='+isdelete+'&userName='+this.state.staffContactFullname+'&endDate='+InactiveEndDate+'&startDate='+InactiveStartDate;
    fetch(url, {
      method: 'PUT',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      //body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
      // console.log("responseJson DeleteUserLocations");
      // console.log(data);
      // debugger;
      if (data.responseType === "1") {
        SystemHelpers.ToastSuccess(data.responseMessge);
        $( ".cancel-btn" ).trigger( "click" );

        this.setState({ IsShowInactiveStartDate: false });
        this.setState({ IsShowInactiveEndDate: false });

        this.GetUsersByLocations(this.state.currentPage,this.state.pageSize,this.state.searchText);
        this.hideLoader();
      }else if (data.responseType == "2" || data.responseType == "3") {
        SystemHelpers.ToastError(data.responseMessge);
        $( ".cancel-btn" ).trigger( "click" );

        this.setState({ IsShowInactiveStartDate: false });
        this.setState({ IsShowInactiveEndDate: false });

        this.hideLoader();
      }else{
        if(data.message == 'Authorization has been denied for this request.'){
          SystemHelpers.SessionOut();
          this.props.history.push("/login");
        }else{
          SystemHelpers.ToastError(data.message);
        }
        this.hideLoader();
        $( ".cancel-btn" ).trigger( "click" );
      }
    })
    .catch(error => {
      console.log('DeleteUserLocations error');
      console.log(error);
      this.props.history.push("/error-500");
    });
  }

  // Pagination Design
  PaginationDesign ()
  {
    let PageOutput = [];
    // console.log('pagination');
    // console.log(this.state.pagingData);
    
    if(this.state.pagingData !="" && this.state.pagingData !="undefined")
    {
      var Page_Count = this.state.pagingData.totalPages;
      //alert(this.state.pagingData.currentPage);
      // console.log('page count = ' + Page_Count);
      /* pagination count */

      var Page_Start=1;
      var Page_End=1;

      if(this.state.pagingData.currentPage == 1){
        Page_Start=1;

        if(Page_Count <= 10){
          Page_End=Page_Count;
        }else{
          Page_End=10;
        }
      }else{
        if(this.state.pagingData.currentPage < 5){
          Page_Start=1;
          Page_End=Page_Count;
          //Page_End=parseInt(this.state.pagingData.currentPage) + parseInt(6);
          // console.log("Page_End 1 "+ Page_End);
        }else{
          Page_Start=parseInt(this.state.pagingData.currentPage) - parseInt(4);
          Page_End=parseInt(this.state.pagingData.currentPage) + parseInt(6);
          // console.log("Page_End 2 "+ Page_End);
          if(Page_End > Page_Count){
            Page_End=Page_Count;
            // console.log("Page_End 3 "+ Page_End);
          }
        }
      }

      let Page = [];
      var i = 1;
      for (var z=Page_Start; z <= Page_End ; z++)
      {
        if(z==this.state.pagingData.currentPage)
        {
          Page.push(<li className="page-item active pk-active">
            <a className="page-link pk-active" id={z} href="#" onClick={this.PageGetGridData}>{z}<span className="sr-only">(current)</span></a>
          </li>);
        }
        else
        {
          Page.push(<li className="page-item"><a className="page-link" id={z} href="#" onClick={this.PageGetGridData} >{z}</a></li>);
        }
        i++;
      }

      let PagePrev = [];

      if(this.state.pagingData.currentPage == 1){
        PagePrev.push(<li className="page-item disabled">
          <a className="page-link" href="#">Previous</a>
        </li>);
      }else{
        PagePrev.push(<li className="page-item">
          <a className="page-link" href="#" id={parseInt(this.state.pagingData.currentPage)-parseInt(1)} tabIndex={-1} onClick={this.PageGetGridData}>Previous</a>
        </li>);
      }

      let PageNext = [];

      if(this.state.pagingData.currentPage == Page_Count){
        PageNext.push(<li className="page-item disabled">
          <a className="page-link" href="#">Next</a>
        </li>);
      }else{
        PageNext.push(
          <li className="page-item">
            <a className="page-link" href="#" id={parseInt(this.state.pagingData.currentPage)+parseInt(1)} onClick={this.PageGetGridData}>Next</a>
          </li>
        );
      }

      let PageLast = [];

      if(this.state.pagingData.currentPage == Page_Count){
        PageLast.push(<li className="page-item disabled">
          <a className="page-link" href="#">Last</a>
        </li>);
      }else{
        PageLast.push(
          <li className="page-item">
            <a className="page-link" href="#" id={parseInt(Page_Count)} onClick={this.PageGetGridData}>Last</a>
          </li>
        );
      }
      
      PageOutput.push(<section className="comp-section" id="comp_pagination">
                        <div className="pagination-box">
                          <div>
                            <ul className="pagination">
                              
                              {PagePrev}
                              {Page}
                              {PageNext}
                              {PageLast}
                              
                            </ul>
                          </div>
                        </div>
                      </section>);
    }
    
    return PageOutput;
  }

  PageGetGridData = e => {

    e.preventDefault();
    let current_page= e.target.id;
    this.GetUsersByLocations(current_page,this.state.pageSize,this.state.searchText)
  }

  SearchGridData = e => {
    this.setState({ pageSize: this.state.TempsearchText });
    this.GetUsersByLocations(1,this.state.pageSize,this.state.TempsearchText);
  }
  // Pagination Design
  
  render() {
     
      return (
        <div>
        {/* Toast & Loder method use */}
            
        {(this.state.loading) ? <Loader /> : null} 
        {/* Toast & Loder method use */}
        <div className="row">
          <div className="col-md-12 d-flex">
            <div className="card profile-box flex-fill">

              <div className="row">
                <button className="btn btn-primary submit-btn pk-profiletab-refreshbtn-hide" id="TabClickOnLoadLocationsDepartments" onClick={this.TabClickOnLoadLocationsDepartments()}>Refresh</button>
              </div>

              <div className="card-body">
                
                {this.state.role_locations_departments_can.locations_departments_can_create == true ?

                <h3 className="card-title">Locations-Departments<a href="#" className="edit-icon" data-toggle="modal" data-target="#LocationsDepartments_Add_modal"><i className="fa fa-plus" /></a></h3>
                  : <h3 className="card-title">Locations-Departments<a href="#" className="phss-lock"><i className="fa fa-lock" /></a></h3>
                }

                {/* Page Per Record and serach design*/}
                  <div className="row filter-row">
                    <div className="col-sm-6 col-md-2"> 
                      <div className="form-group form-focus select-focus">
                        <select className="form-control floating" value={this.state.pageSize}  onChange={this.handleChange('pageSize')}> 
                          <option value="5">5/Page</option>
                          <option value="10">10/Page</option>
                          <option value="50">50/Page</option>
                          <option value="100">100/Page</option>
                        </select>
                        <label className="focus-label">Per Page</label>
                      </div>
                    </div>
                    
                    <div className="col-sm-6 col-md-3">
                      <div className="form-group form-focus focused">
                        <label className="focus-label">Sorting</label>
                        <select className="form-control floating" id="sortColumn" value={this.state.sortColumn} onChange={this.handleChange('sortColumn')}> 
                          {/*<option value="">-</option>*/}
                          <option value="StartDate">Start Date</option>
                          <option value="EndDate">End Date </option>
                          <option value="IsPrimaryLocation">Primary Location</option>
                        </select>
                      </div>
                    </div> 

                    <div className="col-sm-6 col-md-2">
                      <div className="form-group form-focus focused">
                        <label className="focus-label">Sorting Order</label>
                        <select className="form-control floating" id="SortTypeId" value={this.state.SortType} onChange={this.handleChange('SortType')}> 
                          {/*<option value="">-</option>*/}
                          <option value="false">Ascending</option>
                          <option value="true">Descending</option>
                        </select>
                      </div>
                    </div> 

                    <div className="col-sm-6 col-md-3">  
                      <div className="form-group form-focus focused">
                        <input className="form-control floating" type="text" value={this.state.TempsearchText}  onChange={this.handleChange('TempsearchText')} placeholder="Search by Location Name" />
                        <label className="focus-label">Search</label>
                      </div>
                    </div>

                    <div className="col-sm-6 col-md-2">  
                      <a href="#" className="btn btn-success btn-block" onClick={this.SearchGridData}> Search </a>  
                    </div> 
                  </div>
                {/* Page Per Record and serach design*/}

                <div className="row">
                  <div className="col-md-12">
                    <div className="table-responsive">
                      
                      <table className="table table-striped custom-table mb-0 datatable">
                        <thead>
                          { this.state.ListGrid.length > 0 ? this.state.header_data : null}
                        </thead>
                        <tbody>
                          
                          {this.state.ListGrid.length > 0 ? this.state.ListGrid : null}
                        </tbody>
                       </table> 

                      {this.PaginationDesign()}

                    </div>
                  </div>
                </div>
                
              </div>
            </div>
          </div>
        </div>
        {/* ****************** Locations Departments Tab Modals ******************* */}
            {/* Locations Departments Modal */}
            <div id="LocationsDepartments_Add_modal" className="modal custom-modal fade" role="dialog">
              <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div className="modal-content">
                  <div className="modal-header">
                    <h5 className="modal-title">Locations-Departments</h5>
                    <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={this.ClearRecord()} >
                      <span aria-hidden="true">×</span>
                    </button>
                  </div>
                  <div className="modal-body">
                    <form>
                      <div className="card">
                        <div className="card-body">
                          <div className="row">
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Location Name<span className="text-danger">*</span></label>
                                <select className="form-control" value={this.state.AddlocationName} onChange={this.handleChange('AddlocationName')}>
                                  <option value="">-</option>
                                  {this.state.AllLocationName.map(( listValue, index ) => {
                                    return (
                                      <option key={index} value={listValue.locationGuid}>{listValue.locationName}</option>
                                    );
                                  })}
                                </select>
                                <span className="form-text error-font-color">{this.state.errormsg["AddlocationName"]}</span>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Start Date<span className="text-danger">*</span></label>
                                <Datetime
                                inputProps={{readOnly: true}}
                                closeOnTab={true}
                                input={true}
                                value={(this.state.AddStartDate) ? this.state.AddStartDate : ''}
                                onChange={this.handleAddStartDate}
                                dateFormat={process.env.DATE_FORMAT}
                                timeFormat={false}
                                renderInput={(props) => {
                                   return <input {...props} value={(this.state.AddStartDate) ? props.value : ''} />
                                }}
                              />
                                {/*<input className="form-control" type="date" value={this.state.AddStartDate} onChange={this.handleChange('AddStartDate')} />*/}
                                <span className="form-text error-font-color">{this.state.errormsg["AddStartDate"]}</span>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>End Date</label>
                                <Datetime
                                isValidDate={this.validationAddEndDate}
                                inputProps={{readOnly: false}}
                                closeOnTab={true}
                                input={true}
                                value={(this.state.AddEndDate) ? this.state.AddEndDate : ''}
                                onChange={this.handleAddEndDate}
                                dateFormat={process.env.DATE_FORMAT}
                                timeFormat={false}
                                renderInput={(props) => {
                                   return <input {...props} value={(this.state.AddEndDate) ? props.value : ''} />
                                }}
                              />
                                {/*<input className="form-control" type="date" value={this.state.AddEndDate} onChange={this.handleChange('AddEndDate')} min={moment().format(this.state.AddStartDate,"YYYY-MM-DD")} />*/}
                                <span className="form-text error-font-color">{this.state.errormsg["AddEndDate"]}</span>
                              </div>
                            </div>
                          </div>
                          <div className="submit-section">
                            <button className="btn btn-primary submit-btn" onClick={this.AddRecord()}>Submit</button>
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
            {/* //Locations Departments Modal */}

          {/* Locations Departments Ediit Modal */}
            <div id="LocationsDepartments_Edit_modal" className="modal custom-modal fade" role="dialog">
              <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div className="modal-content">
                  <div className="modal-header">
                    <h5 className="modal-title">Locations-Departments</h5>
                    <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">×</span>
                    </button>
                  </div>
                  <div className="modal-body">
                    <form>
                      <div className="card">
                        <div className="card-body">
                          <div className="row">
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Location Name<span className="text-danger">*</span></label>
                                <select className="form-control" value={this.state.EditlocationName} onChange={this.handleChange('EditlocationName')}>
                                  <option value="">-</option>
                                  {this.state.AllLocationName.map(( listValue, index ) => {
                                    return (
                                      <option key={index} value={listValue.locationGuid}>{listValue.locationName}</option>
                                    );
                                  })}
                                </select>
                                <span className="form-text error-font-color">{this.state.errormsg["EditlocationName"]}</span>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Start Date<span className="text-danger">*</span></label>
                                <Datetime
                                  inputProps={{readOnly: true}}
                                  closeOnTab={true}
                                  input={true}
                                  value={(this.state.EditStartDate) ? this.state.EditStartDate : ''}
                                  onChange={this.handleEditStartDate}
                                  dateFormat={process.env.DATE_FORMAT}
                                  timeFormat={false}
                                  renderInput={(props) => {
                                     return <input {...props} value={(this.state.EditStartDate) ? props.value : ''} />
                                  }}
                                />
                                {/*<input className="form-control" type="date" value={this.state.EditStartDate} onChange={this.handleChange('EditStartDate')} />*/}
                                <span className="form-text error-font-color">{this.state.errormsg["EditStartDate"]}</span>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>End Date</label>
                                <Datetime
                                  inputProps={{readOnly: false}}
                                  isValidDate={this.validationEditEndDate}
                                  closeOnTab={true}
                                  input={true}
                                  value={(this.state.EditEndDate) ? this.state.EditEndDate : ''}
                                  onChange={this.handleEditEndDate}
                                  dateFormat={process.env.DATE_FORMAT}
                                  timeFormat={false}
                                  renderInput={(props) => {
                                     return <input {...props} value={(this.state.EditEndDate) ? props.value : ''} />
                                  }}
                                />
                                {/*<input className="form-control" type="date" value={this.state.EditEndDate} onChange={this.handleChange('EditEndDate')} min={moment().format(this.state.EditStartDate,"YYYY-MM-DD")} />*/}
                                <span className="form-text error-font-color">{this.state.errormsg["EditEndDate"]}</span>
                              </div>
                            </div>
                          </div>
                          <div className="submit-section">
                            <button className="btn btn-primary submit-btn" onClick={this.UpdateRecord()}>Update</button>
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          {/* //Locations Departments Edit Modal */}

          {/* Delete Locations Departments Locations  Modal */}
            <div className="modal custom-modal fade" id="LocationsDepartments_Delete_modal" role="dialog">
              <div className="modal-dialog modal-dialog-centered">
                <div className="modal-content">
                  <div className="modal-body">
                    <div className="form-header">
                      <h3>Locations-Departments</h3>
                      <p>Are you sure you want to mark location - department as {this.state.isDelete == true ? 'Active' : 'Inactive' } ?</p>
                    </div>

                    <div className="modal-btn delete-action">
                      {this.state.IsShowInactiveStartDate == true && this.state.isDelete == false ?
                        <div className="row">
                          <div className="form-group">
                            <label>Start Date<span className="text-danger">*</span></label>
                            <Datetime
                            inputProps={{readOnly: true}}
                            closeOnTab={true}
                            input={true}
                            value={(this.state.InactiveStartDate) ? this.state.InactiveStartDate : ''}
                            onChange={this.handleInactiveStartDate}
                            dateFormat={process.env.DATE_FORMAT}
                            timeFormat={false}
                            renderInput={(props) => {
                               return <input {...props} value={(this.state.InactiveStartDate) ? props.value : ''} />
                            }}
                          />
                          <span className="form-text error-font-color">{this.state.errormsg["InactiveStartDate"]}</span>
                          </div>
                        </div>:
                        null
                      }
                      {this.state.IsShowInactiveEndDate == true && this.state.isDelete == false ?
                        <div className="row">
                          <div className="form-group">
                            <label>End Date<span className="text-danger">*</span></label>
                            <Datetime
                            isValidDate={this.validationInactiveEndDate}
                            inputProps={{readOnly: true}}
                            closeOnTab={true}
                            input={true}
                            value={(this.state.InactiveEndDate) ? this.state.InactiveEndDate : ''}
                            onChange={this.handleInactiveEndDate}
                            dateFormat={process.env.DATE_FORMAT}
                            timeFormat={false}
                            renderInput={(props) => {
                               return <input {...props} value={(this.state.InactiveEndDate) ? props.value : ''} />
                            }}
                          />
                          <span className="form-text error-font-color">{this.state.errormsg["InactiveEndDate"]}</span>
                          </div>
                        </div>:
                        null
                      }

                    </div>

                    <div className="modal-btn delete-action">
                      <div className="row">
                        <div className="col-6">
                          <a  onClick={this.DeleteRecord()} className="btn btn-primary continue-btn">{this.state.isDelete == true ? 'Active' : 'Inactive' }</a>
                        </div>
                        <div className="col-6">
                          <a href="" data-dismiss="modal" className="btn btn-primary cancel-btn">Cancel</a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          {/* /Delete Locations Departments Modal */}


            {/* /****************** Locations Departments Tab Modals ******************* */}
        </div>
      );
   }
}

export default ProfileLocation;
