/**
 * Signin Firebase
 */

import React, { Component } from 'react';
import { Helmet } from "react-helmet";
import Consentsandwaivers from "./ConsentsAwards/ConsentsAndWaivers";
import AwardsAndRecognition from "./ConsentsAwards/AwardsAndRecognition";
import SystemHelpers from '../../Helpers/SystemHelper';

import CryptoAES from 'crypto-js/aes';
import CryptoENC from 'crypto-js/enc-utf8';

class ConsentsAwards extends Component {
constructor(props) {
    super(props);

    this.state = {
        errormsg :  '',
        user_role: [],
        staffContactID:this.props.staffContactID
    };
    this.setPropState = this.setPropState.bind(this);
}

setPropState(key, value) {
      this.setState({ [key]: value });
}

componentDidMount() {
    // console.log("consents_waivers  awards_recognitions");
    // console.log(this.props.consents_waivers_can_create);
    // console.log(this.props.consents_waivers_can_update);
    // console.log(this.props.consents_waivers_can_view);
    // console.log(this.props.consents_waivers_can_delete);
    // console.log(this.props.consents_waivers_can_approve);
    // console.log(this.props.consents_waivers_can_export);

    // console.log(this.props.awards_recognitions_can_create);
    // console.log(this.props.awards_recognitions_can_update);
    // console.log(this.props.awards_recognitions_can_view);
    // console.log(this.props.awards_recognitions_can_delete);
    // console.log(this.props.awards_recognitions_can_approve);
    // console.log(this.props.awards_recognitions_can_export);
}
   render() {
     
      return (
        <div>
          <Consentsandwaivers 
            staffContactID={this.state.staffContactID}
            primaryLocationId={this.props.primaryLocationId}

          	consents_waivers_can={this.props.consents_waivers_can}
            
            setPropState={this.setPropState}
          />
          
        </div>
      );
   }
}

export default ConsentsAwards;
