/**
 * Signin Firebase
 */

import React, { Component } from 'react';
import { Helmet } from "react-helmet";

import moment from 'moment';

import Loader from '../../Loader';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import SystemHelpers from '../../Helpers/SystemHelper';

//table
import { MDBDataTable } from 'mdbreact';
import '@fortawesome/fontawesome-free/css/all.min.css';
import 'bootstrap-css-only/css/bootstrap.min.css';
import 'mdbreact/dist/css/mdb.css';
//table

import CryptoAES from 'crypto-js/aes';
import CryptoENC from 'crypto-js/enc-utf8';

import Datetime from "react-datetime";

import Entitlements from "../../PHSSPage/Entitlements";

class ProfileLocation extends Component {
  constructor(props) {
    super(props);

    this.state = {

        // Pagination
        totalCount : 0,
        pageSize : 5,
        currentPage : 1,
        totalPages : 0,
        previousPage : false,
        nextPage : false,
        searchText : '',
        pagingData : {},
        TempsearchText:'',

        sortColumn : '',
        SortType : false,
        IsSortingEnabled : true,
        // Pagination
       
        errormsg : '',
         
        staffContactID:this.props.staffContactID,
        staffContactName:localStorage.getItem("fullName"),

        role_entitlement_can: {},

        HistoryListGrid:[],

        HistoryYearList:[],

        // Add time Dropdown list
        EntitlementYearList : [],
        EntitlementCategoryList : [],
        EntitlementSubCategoryList : [],
        // Add time Dropdown list

        // Search
        // Search

        // History 
        HistoryYear : localStorage.getItem('LocalStorageCurrentYearGuid'),

        HistoryEntitlementsCurrentYear : '',
        HistoryEntitlementsCurrentVacation : '',
        HistoryEntitlementsCurrentSickTime : '',
        HistoryEntitlementsVacationBalanceFor : '',

        HistoryEntitlementsCarryYear : '',
        HistoryEntitlementsCarryVacation : '',
        HistoryEntitlementsCarryOverTime : '',

        HistoryContactId : '',

        HistoryCovidEntitlementsFor : '',
        HistoryCovidBalanceUpToDate : '',
        // History

        // Add
        AddentitlementYear : '',
        AddentitlementCategory : '',
        AddentitlementSubCategory : '',
        AddentitlementAction : '',
        AddentitlementQtyhours : '',
        AddentitlementQtyminutes : '',
        AddentitlementDescription : '',

        AddentitlementAppliesTos : [],

        AddApplyContactField : 'Contact',
        AddApplyContactOperator : '',
        AddApplyContactValue : '',
        // Add

        isDelete : false,
        
        
    };
    this.setPropState = this.setPropState.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  setPropState(key, value) {
      this.setState({ [key]: value });
  }

  handleChange = input => e => {
    this.setState({ [input]: e.target.value });
    if (this.state[input] != '') {
        delete this.state.errormsg[input];
    }

    // Year Change event
    if([input]=="HistoryYear")
    {
      this.setState({ HistoryYear : e.target.value });
      console.log("HistoryYear="+e.target.value);
      this.GetUsersEntitlementBalanceVieWHistory(e.target.value);
    }
    // Year Change event

    // Add time
    if([input]=="AddentitlementCategory")
    {
      this.setState({ EntitlementSubCategoryList: [] });
    
      this.GetEntitlementSubCategoryView(e.target.value);
      this.setState({ AddentitlementSubCategory: '' });
    }
    // Add time
  }

  // Loader hide show method
  hideLoader = () => {
    this.setState({ loading: false });
  }

  showLoader = () => {
    this.setState({ loading: true });
  }
  // Loader hide show method

  componentDidMount() {

    /* Role Management */
    console.log('Role Store entitlement_can');
    console.log(this.props.entitlement_can);
    let entitlement_can = this.props.entitlement_can;
    this.setState({ role_entitlement_can: this.props.entitlement_can });
    /* Role Management */

    this.GetEntitlementCategoryView();
    this.GetPhssYears();
    this.GetUsersEntitlementBalanceVieWHistory(this.state.HistoryYear);
  }

  // Drop down Api
  GetEntitlementCategoryView(){
    this.showLoader();
    var url=process.env.API_API_URL+'GetEntitlementCategoryView';
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
    }).then((response) => response.json())
    .then(data => {
        //console.log("responseJson GetEntitlementCategoryView");
        //console.log(data);
        if (data.responseType === "1") {
          this.setState({ EntitlementCategoryList: data.data});
        }else{
          SystemHelpers.ToastError(data.message);   
        }
        this.hideLoader();
    })
    .catch(error => {
      this.props.history.push("/error-500");
    });
  }

  GetEntitlementSubCategoryView(id){
    this.showLoader();
    var url=process.env.API_API_URL+'GetEntitlementSubCategoryView?categoryId='+id;
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
    }).then((response) => response.json())
    .then(data => {
        //console.log("responseJson GetEntitlementSubCategoryView");
        //console.log(data);
        if (data.responseType === "1") {
          this.setState({ EntitlementSubCategoryList: data.data});
        }else{
          SystemHelpers.ToastError(data.message);   
        }
        this.hideLoader();
        
    })
    .catch(error => {
      this.props.history.push("/error-500");
    });
  }

  GetPhssYears(){
    this.showLoader();
    var url=process.env.API_API_URL+'GetPhssYears';
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
    }).then((response) => response.json())
    .then(data => {
        console.log("responseJson GetPhssYears");
        console.log(data);
        if (data.responseType === "1") {
          this.setState({ EntitlementYearList: data.data});
          this.setState({ HistoryYearList: data.data});
        }else{
          SystemHelpers.ToastError(data.message);   
        }
        this.hideLoader();
    })
    .catch(error => {
      this.props.history.push("/error-500");
    });
  }
  // Drop down Api

  GetUsersEntitlementBalanceVieWHistory (YearId) {
  //GetUsersEntitlementBalanceVieWHistory = (YearId) => e => {

    //e.preventDefault();
      
      this.setState({ HistoryListGrid: [] });

      /* Role Management */
      var getrole = SystemHelpers.GetRole();
      //console.log('Profile Entitlement Get role');
      /* Role Management */

      //var SearchYear = this.state["HistoryYear"];
      var SearchYear = YearId;
      console.log("get api="+SearchYear);
      
      this.showLoader();
      var url=process.env.API_API_URL+'GetUsersEntitlementBalanceVieWHistory?contactId='+this.state.staffContactID+'&yearId='+SearchYear;
      fetch(url, {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'bearer '+localStorage.getItem("token")
        },
      }).then((response) => response.json())
      .then(data => {
          //console.log("responseJson GetUsersEntitlementBalanceVieWHistory");
          //console.log(data);
          if (data.responseType == "1") {
            
            this.setState({ HistoryListGrid: this.rowData(data.data) });
            
          }else{
            SystemHelpers.ToastError(data.message); 
          }
          this.hideLoader();
      })
      .catch(error => {
        console.log("catch");
        console.log(error);
        this.props.history.push("/error-500");
      });
  }

  rowData(HistoryListGrid) {
      console.log('row Entitlement');
      console.log(HistoryListGrid);
      //console.log(HistoryListGrid.length);
      
      

      
      /* Role Management */
      var getrole = SystemHelpers.GetRole();
      let canDelete = getrole.scheduler_can.scheduler_can_delete;
      /* Role Management */

      this.setState({ HistoryContactId: HistoryListGrid.contactId});

      //this.setState({ HistoryYear: this.state["HistoryYear"]});

      // Entitlements Current Year

        // Entitlements Year
        let yearsList= this.state.HistoryYearList;
        var YearHistory = this.state.HistoryYear;

        var length = yearsList.length;
        
        if (length > 0) {
          for (var zz = 0; zz < length; zz++) {
            if(yearsList[zz].guidId === YearHistory){
              var current_year=yearsList[zz].name;
              this.setState({ HistoryEntitlementsCurrentYear: current_year });
            }
          }
          zz++;
        }
        // Entitlements Year

      this.setState({ HistoryEntitlementsCurrentVacation: HistoryListGrid.vacationHoursCurrent+'h'});
      
      this.setState({ HistoryEntitlementsCurrentSickTime: HistoryListGrid.sickTimeHoursCurrent+'h'});

      this.setState({ HistoryEntitlementsVacationBalanceFor: HistoryListGrid.vacationTotal+'h'});
      // Entitlements Current Year

      // Carried Forward Year
      var CarryForward_year=this.state.HistoryEntitlementsCurrentYear;
      this.setState({ HistoryEntitlementsCarryYear: moment(CarryForward_year).subtract(1, 'year').format(process.env.ENTITLEMENTS_DATE_FORMAT)});
      
      this.setState({ HistoryEntitlementsCarryVacation: HistoryListGrid.vacationHoursCarryForward+'h'});
      
      this.setState({ HistoryEntitlementsCarryOverTime: HistoryListGrid.overTimeTotal+'h'});
      // Carried Forward Year

      // COVID
      this.setState({ HistoryCovidEntitlementsFor: 0+'d'});
      this.setState({ HistoryCovidBalanceUpToDate: HistoryListGrid.overTimeTotal+'d'});
      // COVID

      // History Grid
      
      let dataArray = [];
      var i=1;
      if( HistoryListGrid.userEntitlementViews!=null && HistoryListGrid.userEntitlementViews!= "")
      {
        var HistoryListGrid_length = HistoryListGrid.userEntitlementViews.length;
        
        if(HistoryListGrid_length> 0 )
        {
          for (var z = 0; z < HistoryListGrid_length; z++) 
          {
            var tempdataArray = [];
            
            tempdataArray.date = HistoryListGrid.userEntitlementViews[z].transactionDate;
            tempdataArray.description = HistoryListGrid.userEntitlementViews[z].description;

            var vacation_history= HistoryListGrid.userEntitlementViews[z].vacationHours;
            if(vacation_history.match(/-/)){
              tempdataArray.vacationhours = <div style={{'color' : 'red'}}>{vacation_history}</div>;
            }else{
              tempdataArray.vacationhours = <div>{vacation_history}</div>;
            }
            
            var sickTimeHours_history= HistoryListGrid.userEntitlementViews[z].sickTimeHours;
            if(sickTimeHours_history.match(/-/)){
              tempdataArray.sicktimehours = <div style={{'color' : 'red'}}>{sickTimeHours_history}</div>;
            }else{
              tempdataArray.sicktimehours = <div>{sickTimeHours_history}</div>;
            }
            
            var overtimeHours_history = HistoryListGrid.userEntitlementViews[z].overtimeHours;
            if(overtimeHours_history.match(/-/)){
              tempdataArray.overtimehours = <div style={{'color' : 'red'}}>{overtimeHours_history}</div>;
            }else{
              tempdataArray.overtimehours = <div>{overtimeHours_history}</div>;
            }

            var otherLeaves_history= HistoryListGrid.userEntitlementViews[z].otherLeaves;
            if(otherLeaves_history.match(/-/)){
              tempdataArray.leaves = <div style={{'color' : 'red'}}>{otherLeaves_history}</div>;
            }else{
              tempdataArray.leaves = <div>{otherLeaves_history}</div>;
            }

            
            dataArray.push(tempdataArray);
            i++;
          }

        }
      }
      // History Grid
      
      //console.log('Row Entitlement return');
      //console.log(dataArray);
      return dataArray;
  }

  ClearRecord = ()=> e => {
    e.preventDefault();

    this.setState({ AddentitlementYear: '' });
    this.setState({ AddentitlementCategory: '' });
    this.setState({ AddentitlementSubCategory: '' });
    this.setState({ AddentitlementAction: '' });
    this.setState({ AddentitlementQtyhours: '' });
    this.setState({ AddentitlementQtyminutes: '' });
    this.setState({ AddentitlementDescription: '' });

    this.setState({ errormsg: '' });
  }

  AddRecord = () => e => {
      e.preventDefault();

      let step1Errors = {};

      if (this.state["AddentitlementYear"] == '' || this.state["AddentitlementYear"] == null) {
        step1Errors["AddentitlementYear"] = "Year is mandatory";
      }

      if (this.state["AddentitlementCategory"] == '' || this.state["AddentitlementCategory"] == null) {
        step1Errors["AddentitlementCategory"] = "Entitlement Category is mandatory";
      } 
      
      if (this.state["AddentitlementAction"] == '' || this.state["AddentitlementAction"] == null) {
        step1Errors["AddentitlementAction"] = "Action is mandatory";
      }

      if (this.state["AddentitlementQtyhours"] == '' || this.state["AddentitlementQtyhours"] == null) {
        step1Errors["AddentitlementQtyhours"] = "Hours is mandatory";
      }

      if (this.state["AddentitlementQtyminutes"] == '' || this.state["AddentitlementQtyminutes"] == null) {
        step1Errors["AddentitlementQtyminutes"] = "Minutes is mandatory";
      }
      
      if (this.state["AddentitlementSubCategory"] == '' || this.state["AddentitlementSubCategory"] == null) {
        step1Errors["AddentitlementSubCategory"] = "Reason is mandatory";
      }

      if (this.state["AddentitlementDescription"] == '' || this.state["AddentitlementDescription"] == null) {
        step1Errors["AddentitlementDescription"] = "Description is mandatory";
      }
      
      //console.log(step1Errors);

      this.setState({ errormsg: step1Errors });
          
      if (Object.keys(step1Errors).length > 0) {
        return false;
      }

      this.showLoader();
      
      let bodyarray = {};
      bodyarray["loggedInUserId"] = this.state.staffContactID;
      bodyarray["yearId"] = this.state["AddentitlementYear"];
      bodyarray["entitlementCategoryId"] = this.state["AddentitlementCategory"];
      bodyarray["entitlementAction"] = this.state["AddentitlementAction"];
      bodyarray["entitlementQty"] = this.state["AddentitlementQtyhours"]+':'+this.state["AddentitlementQtyminutes"];
      bodyarray["entitlementSubCategoryId"] = this.state["AddentitlementSubCategory"];
      bodyarray["entitlementDescription"] = this.state["AddentitlementDescription"];

      let ArrayJson=[{
        appliesToFieldId : "C",
        operator : "=",
        appliesToFieldValue : this.state.staffContactID,
      }];
      bodyarray["entitlementAppliesTos"] = ArrayJson;
      
      var url=process.env.API_API_URL+'PostEntitlement';
      fetch(url, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'bearer '+localStorage.getItem("token")
        },
        body: JSON.stringify(bodyarray)
      }).then((response) => response.json())
      .then(data => {
          //console.log("responseJson PostEntitlement");
          //console.log(data);
          if (data.responseType === "1") {
            SystemHelpers.ToastSuccess(data.responseMessge);  
            $( ".close" ).trigger( "click" ); 
            
            this.setState({ AddentitlementYear: '' });
            this.setState({ AddentitlementCategory: '' });
            this.setState({ AddentitlementSubCategory: '' });
            this.setState({ AddentitlementAction: '' });
            this.setState({ AddentitlementQtyhours: '' });
            this.setState({ AddentitlementQtyminutes: '' });
            this.setState({ AddentitlementDescription: '' });

            this.setState({ errormsg: '' });

            this.GetUsersEntitlementBalanceVieWHistory(this.state.HistoryYear);
          }else if (data.responseType === "2" || data.responseType === "3" || data.responseType === "4") {
            SystemHelpers.ToastError(data.responseMessge);  
          } else{
            SystemHelpers.ToastError(data.message);
          }
          this.hideLoader();
      })
      .catch(error => {
        this.props.history.push("/error-500");
      });
  }
  
  render() {
      const data = {
        columns: [
        {
          label: 'Date',
          field: 'date',
          sort: 'asc',
          width: 150
        },
        {
          label: 'Description',
          field: 'description',
          sort: 'asc',
          width: 150
        },
        {
          label: 'Vacation hours',
          field: 'vacationhours',
          sort: 'asc',
          width: 150
        },
        {
          label: 'Overtime hours',
          field: 'overtimehours',
          sort: 'asc',
          width: 150
        },
        {
          label: 'Sick time hours',
          field: 'sicktimehours',
          sort: 'asc',
          width: 100
        },
        {
          label: 'Leaves',
          field: 'leaves',
          sort: 'asc',
          width: 100
        }
      ],
      rows: this.state.HistoryListGrid
    };

      return (
        <div>
        {/* Toast & Loder method use */}
            
        {(this.state.loading) ? <Loader /> : null} 
        {/* Toast & Loder method use */}
        <div className="row">
          <div className="col-md-12 d-flex">
            <div className="card profile-box flex-fill">
              <div className="card-body">
                
                <h3 className="card-title">Entitlements - Current Balance</h3>
                
                
                {/* Search & View */}
                <div className="row filter-row">

                  <div className="col-md-2">
                    <div className="form-group">
                      <label>Year</label>
                      <select className="form-control floating" value={this.state.HistoryYear} onChange={this.handleChange('HistoryYear')}> 
                        <option value="">-</option>
                        {this.state.HistoryYearList.map(( listValue, index ) => {
                            return (
                              <option key={index} value={listValue.guidId} >{listValue.name}</option>
                            );
                        })}
                      </select>
                      <span className="form-text error-font-color">{this.state.errormsg["HistoryYear"]}</span>
                    </div>
                  </div>
                  <div className="col-md-7">
                    <div className="form-group">
                    </div>
                  </div>
                  <div className="col-md-3">
                    <div className="form-group">
                      <label></label>
                      {/*<input className="form-control floating" type="text"/>*/}
                    </div>
                  </div>

                  <div className="col-md-9">
                    <div className="form-group">
                    </div>
                  </div>
                  <div className="col-md-3">
                    <div className="form-group">
                      {this.state.role_entitlement_can.entitlement_can_create == true ?

                        <a href="#" className="btn btn-primary mr-2 float-right" data-toggle="modal" data-target="#add_entitlement">Add Entitlement</a>
                          : null
                        }
                    </div>
                  </div>

                  <div className="col-md-4">
                    <div className="form-group">
                      <label><b>Carried forward {this.state.HistoryEntitlementsCarryYear}:</b></label>
                    </div>
                  </div>
                  <div className="col-md-4">
                    <div className="form-group">
                      <label><b>Entitlements {this.state.HistoryEntitlementsCurrentYear}:</b></label>
                    </div>
                  </div>
                  <div className="col-md-4">
                    <div className="form-group">
                    </div>
                  </div>

                  <div className="col-md-2">
                    <div className="form-group">
                    <label><b>Vacation:</b> {this.state.HistoryEntitlementsCarryVacation}</label>
                    </div>
                  </div>
                  <div className="col-md-2">
                    <div className="form-group">
                      <label><b>Overtime:</b> {this.state.HistoryEntitlementsCarryOverTime}</label>
                    </div>
                  </div>
                  <div className="col-md-2">
                    <div className="form-group">
                      <label><b>Vacation:</b> {this.state.HistoryEntitlementsCurrentVacation}</label>
                    </div>
                  </div>
                  <div className="col-md-2">
                    <div className="form-group">
                      <label><b>Sick Time:</b> {this.state.HistoryEntitlementsCurrentSickTime}</label>
                    </div>
                  </div>
                  <div className="col-md-3">
                    <div className="form-group">
                    <label><b>COVID entitlements for {this.state.HistoryEntitlementsCurrentYear}:</b> {this.state.HistoryCovidEntitlementsFor}</label>
                    </div>
                  </div>

                  <div className="col-md-4">
                    <div className="form-group">
                    </div>
                  </div>
                  <div className="col-md-4">
                    <div className="form-group">
                      <label><b>Vacation Balance for {this.state.HistoryEntitlementsCurrentYear}:</b> {this.state.HistoryEntitlementsVacationBalanceFor}</label>
                    </div>
                  </div>
                  <div className="col-md-4">
                    <div className="form-group">
                      <label><b>COVID Balance up to date:</b> {this.state.HistoryCovidBalanceUpToDate}</label>
                    </div>
                  </div>

                </div>
                {/* Search & View */}

                {/* Table */}
                <div className="row"></div>
                {/*<div className="table-responsive">
                  <MDBDataTable
                    striped
                    bordered
                    small
                    data={data}
                    entriesOptions={[5, 10, 20, 50, 100]} entries={5} pagesAmount={4}
                    className="table table-striped custom-table mb-0 datatable"
                  />

                </div>*/}
                <div className="card-body">
                      <div className="row">
                          <div className="col-md-2">
                            <div className="table-responsive">
                              
                              <table className="table table-bordered mb-0 pk-profileEntitlement-table-to-border">
                                <thead>
                                  <tr>
                                    <th>Date</th>
                                    <th>Description</th>
                                    <th>Vacation hours</th>
                                    <th>Overtime hours</th>
                                    <th>Sick time hours</th>
                                    <th>Leaves</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  {this.state.HistoryListGrid.map(( listValue, index ) => {
                                      
                                      return (
                                        <tr key={index}>
                                          <td>{listValue.userEntitlementViews.transactionDate}</td>
                                          <td></td>
                                          <td></td>
                                          <td></td>
                                          <td></td>
                                          <td></td>
                                          <td></td>
                                          
                                          <td></td>
                                        </tr>
                                      );
                                    
                                  })}
                                  <tr><td></td></tr>
                                </tbody>
                              </table>
                              </div>
                          </div>
                      </div>
                </div>
                {/* Table */}
              </div>
            </div>
          </div>
        </div>
        {/* ****************** Profile Entitlement Tab Modals ******************* */}
          {/* Entitlement Modal */}
            {/*<Entitlements 
                staffContactID={this.state.staffContactID}
                entitlement_can={this.state.role_entitlement_can}
                page_type="profile_entitlement_add"
                setPropState={this.setPropState}
                ProfileYearID={this.state.HistoryYear}
              />*/}
            <div id="add_entitlement" data-backdrop="static" className="modal custom-modal" role="dialog">
                <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
                  <div className="modal-content">
                    <div className="modal-header">
                      <h5 className="modal-title">Create a Entitlements</h5>
                      <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={this.ClearRecord()}>
                        <span aria-hidden="true">×</span>
                      </button>
                    </div>
                    <div className="modal-body">
                      <div className="modal-content">
                        <div className="modal-body">
                          <form>
                            <div className="row">

                              <div className="col-md-4">
                                <div className="form-group">
                                  <label>Year<span className="text-danger">*</span></label>
                                  <select className="form-control floating" id="AddentitlementYear" value={this.state.AddentitlementYear} onChange={this.handleChange('AddentitlementYear')}> 
                                    <option value="">-</option>
                                    {this.state.EntitlementYearList.map(( listValue, index ) => {
                                     
                                        return (
                                          <option key={index} value={listValue.guidId} >{listValue.name}</option>
                                        );
                                     
                                    })}
                                  </select>
                                  <span className="form-text error-font-color">{this.state.errormsg["AddentitlementYear"]}</span>
                                </div>
                              </div>

                              <div className="col-md-4">
                                <div className="form-group">
                                  <label>Entitlement Category<span className="text-danger">*</span></label>
                                  <select className="form-control floating" id="AddentitlementCategory" value={this.state.AddentitlementCategory} onChange={this.handleChange('AddentitlementCategory')}> 
                                    <option value="">-</option>
                                    {this.state.EntitlementCategoryList.map(( listValue, index ) => {
                                     
                                        return (
                                          <option key={index} value={listValue.entitlementCategoryId} >{listValue.entitlementCategoryName}</option>
                                        );
                                     
                                    })}
                                  </select>
                                  <span className="form-text error-font-color">{this.state.errormsg["AddentitlementCategory"]}</span>
                                </div>
                              </div>

                              <div className="col-md-4">
                                <div className="form-group">
                                  <label>Action<span className="text-danger">*</span></label>
                                  <select className="form-control floating" id="AddentitlementAction" value={this.state.AddentitlementAction} onChange={this.handleChange('AddentitlementAction')}> 
                                    <option value="">-</option>
                                    <option value="Add">Add</option>
                                    <option value="Subtract">Subtract</option>
                                  </select>
                                  <span className="form-text error-font-color">{this.state.errormsg["AddentitlementAction"]}</span>
                                </div>
                              </div>

                              

                              <div className="col-md-6">
                                <div className="form-group">
                                  <label>Reason<span className="text-danger">*</span></label>
                                  <select className="form-control floating" id="AddentitlementSubCategory" value={this.state.AddentitlementSubCategory} onChange={this.handleChange('AddentitlementSubCategory')}> 
                                    <option value="">-</option>
                                    {this.state.EntitlementSubCategoryList.map(( listValue, index ) => {
                                     
                                        return (
                                          <option key={index} value={listValue.entitlementSubCategoryId} >{listValue.entitlementSubCategoryName}</option>
                                        );
                                     
                                    })}
                                  </select>
                                  <span className="form-text error-font-color">{this.state.errormsg["AddentitlementSubCategory"]}</span>
                                </div>
                              </div>
                              

                              <div className="col-md-2">
                                <div className="form-group">
                                  <label>Hours<span className="text-danger">*</span></label>
                                  <input type="number" className="form-control" value={this.state.AddentitlementQtyhours} onChange={this.handleChange('AddentitlementQtyhours')} />
                                  <span className="form-text error-font-color">{this.state.errormsg["AddentitlementQtyhours"]}</span>
                                </div>
                              </div>
                              <div className="col-md-2">
                                <div className="form-group">
                                  <label>Minutes<span className="text-danger">*</span></label>
                                  <select className="form-control floating" value={this.state.AddentitlementQtyminutes} onChange={this.handleChange('AddentitlementQtyminutes')}> 
                                    <option value="">-</option>
                                    <option value="00">00</option>
                                    <option value="15">15</option>
                                    <option value="30">30</option>
                                    <option value="45">45</option>
                                  </select>
                                  <span className="form-text error-font-color">{this.state.errormsg["AddentitlementQtyminutes"]}</span>
                                </div>
                              </div>
                              <div className="col-md-2">
                                <div className="form-group">
                                  <label>Unit<span className="text-danger">*</span></label>
                                  <input type="text" className="form-control" disabled value="hours" onChange={this.handleChange('AddentitlementUnit')} style={{"background-color" : "#f6f6f6"}} />
                                  <span className="form-text error-font-color"></span>
                                </div>
                              </div>

                              <div className="col-md-12">
                                <div className="form-group">
                                  <label>Description<span className="text-danger">*</span></label>
                                  <textarea rows="3" cols="5" class="form-control" value={this.state.AddentitlementDescription} onChange={this.handleChange('AddentitlementDescription')}></textarea>
                                  <span className="form-text error-font-color">{this.state.errormsg["AddentitlementDescription"]}</span>
                                </div>
                              </div>

                              
                            </div>

                            <div className="submit-section">
                              <button className="btn btn-primary submit-btn">Cancel</button>
                              <button className="btn btn-primary submit-btn" onClick={this.AddRecord()}>Submit</button>
                            </div>

                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
          {/* //Entitlement Modal */}

          {/* Entitlement Ediit Modal */}
            
          {/* //Entitlement Edit Modal */}
        {/* /****************** Profile Entitlement Tab Modals ******************* */}
        </div>
      );
   }
}

export default ProfileLocation;
