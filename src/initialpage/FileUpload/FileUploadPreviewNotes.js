import React, { Component, Fragment } from 'react';
import ReactDOM from 'react-dom';
import { Upload } from '@progress/kendo-react-upload';
import '@progress/kendo-theme-default/dist/all.css';
import DocxImg from '../../assets/img/doc/docx.png'
import ExcelImg from '../../assets/img/doc/excel.png'
import PdfImg from '../../assets/img/doc/pdf.png'
import $ from 'jquery';
const fileStatuses = [
    'UploadFailed',
    'Initial',
    'Selected',
    'Uploading',
    'Uploaded',
    'RemoveFailed',
    'Removing'
];

class FileUploadPreview extends Component {
   
    constructor(props) {
        super(props);

        this.state = {
            files: [],
            events: [],
            filePreviews: {},
            filePreviewsFinal: []
        };
        this.setPropState = this.setPropState.bind(this);
    }

    componentDidMount() {
        console.log();
        $('.removeDocumentFunc').hide();
    }

    setPropState(key, value) {
        this.setState({ [key]: value });
    }

    base64MimeType(encoded) {
      var result = null;

      if (typeof encoded !== 'string') {
        return result;
      }

      var mime = encoded.match(/data:([a-zA-Z0-9]+\/[a-zA-Z0-9-.+]+).*,.*/);

      if (mime && mime.length) {
        result = mime[1];
      }

      return result;
    }

    
    onAdd = (event) => {
        console.log("pk");
        console.log(event.affectedFiles);

        /* pk Validation Add*/
        const file_pk = event.affectedFiles[0];
        const reader_pk = new FileReader();
        let image_width = 0;
        let image_height = 0;

        reader_pk.readAsDataURL(file_pk.getRawFile());

        reader_pk.onload = function (e) {
            var img = new Image;
            img.onload = function() {
                console.log("The width of the image is " + img.width + "px." + img.height + "px.");
                image_width = img.width;
                image_height = img.height;
                
                
                // if(img.width != 800 || img.height != 533){
                //     alert("please upload an image with 800x533 pixels dimension");
                //     return false;
                // }
                
            };
            img.src = reader_pk.result;
        };


        


        /* pk Validation Add*/
            var current_upload=this.state.filePreviewsFinal.length;
            var current_upload1=$("#filePreviewsFinalEditActive").val();
            var new_current_upload=event.affectedFiles.length;
            //alert(current_upload + " + " + new_current_upload);
            var total_upload=parseInt(current_upload1)+parseInt(current_upload)+parseInt(new_current_upload);
            //alert(total_upload);

            if(total_upload > 6){
                alert("You can upload maximum 6 Files.");
                return false;
            }

            const files = new Map();

            const afterStateChange = () => {
                var i = 0;
                event.affectedFiles
                    .filter(file => !file.validationErrors)
                    .forEach(file => {
                        const reader = new FileReader();
                       
                        files.set(reader, file);

                        reader.onloadend = (ev) => {

                            let ArrayJson = {
                                FileData: ev.target.result,
                                FileName: file.name,
                            };
                            
                            this.setState({
                                filePreviews: {
                                    ...this.state.filePreviews,
                                    [file.uid]: ArrayJson
                                }
                            });
                            i++;
                        };


                        reader.readAsDataURL(file.getRawFile());

                        // reader.onload = function (e) {
                        //     var img = new Image;
                        //     img.onload = function() {
                        //       console.log("The width of the image is " + img.width + "px.");
                        //     };
                        //     img.src = reader.result;
                        // };
                    });
                   
            };

            this.setState({
                files: event.newState,
                events: [
                    ...this.state.events,
                    `File selected: ${event.affectedFiles[0].name}`
                ]
            }, afterStateChange);

        

    }

    onRemove = (event) => {
        const filePreviews = {
            ...this.state.filePreviews
        };

        event.affectedFiles.forEach(file => {
            delete filePreviews[file.uid];
        });

        this.setState({
            files: event.newState,
            events: [
                ...this.state.events,
                `File removed: ${event.affectedFiles[0].name}`
            ],
            filePreviews: filePreviews
        });

        /* ***  pk code **** */
        let temp = [];
        Object.keys(this.state.filePreviews).map((fileKey) => (
            temp.push({'FileData':this.state.filePreviews[fileKey].FileData,'FileName':this.state.filePreviews[fileKey].FileName}) 
        ))
        this.setState({ filePreviewsFinal: temp })
        this.props.setPropState('filePreviewsFinal', temp);
        /* ***  pk code **** */
    }

    onProgress = (event) => {
        this.setState({
            files: event.newState,
            events: [
                ...this.state.events,
                `On Progress: ${event.affectedFiles[0].progress} %`
            ]
        });
    }

    onStatusChange = (event) => {
        
        const file = event.affectedFiles[0];

        /* ***  pk code **** */
        let temp = [];
        Object.keys(this.state.filePreviews).map((fileKey) => (
            temp.push({'FileData':this.state.filePreviews[fileKey].FileData,'FileName':this.state.filePreviews[fileKey].FileName})   
        ))
        this.setState({ filePreviewsFinal: temp })
        this.props.setPropState('filePreviewsFinal', temp);
        /* ***  pk code **** */
        this.setState({
            files: event.newState,
            events: [
                ...this.state.events,
                `File '${file.name}' status changed to: ${fileStatuses[file.status]}`
            ]
        });
    }

    randomFun()
    {
        var min = 100;
        var max = 999;
        return Math.floor(Math.random()*(max-min+1)+min);
    }
    GetFileName(baseurl){
        var imgbase=baseurl;
        var gettype=this.base64MimeType(baseurl);

        var ext='';
        
        var today = new Date();
        var y = today.getFullYear();
        var m = today.getMonth() + 1;
        var d = today.getDate();
        var h = today.getHours();
        var mi = today.getMinutes();
        var s = today.getSeconds();
        var ms = today.getMilliseconds();
        var time = "PHSS000"+y  + m  + d  + h  + mi  + s + ms+this.randomFun();
        if(gettype == 'image/png'){
            ext='.png';
        }else if(gettype == 'image/jpeg'){
            ext='.jpg';
        }else if(gettype == 'image/jpg'){
            ext='.jpg';
        }else if(gettype == 'application/vnd.ms-excel'){
            ext='.csv';
        }else if(gettype == 'application/pdf'){
            ext='.pdf';
        }else if(gettype == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'){
            ext='.xlsx';
        }else if(gettype == 'application/vnd.ms-excel'){
            ext='.xls';
        }else if(gettype == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'){
            ext='.docx';
        }else if(gettype == 'application/msword'){
            ext='.doc';
        }

        var file_name=time+ext

        
        return file_name;
    }

    resetFile(){
        if(this.props.ResetFileMethod == true ){
            this.props.setPropState('ResetFileMethod', false);
            $( ".removeDocumentFunc" ).trigger( "click" );
        }
    }

    removeFunc = () => e => {
        e.preventDefault();
        //alert();
        this.setState({ files: [] });
        this.setState({ events: [] });
        this.setState({ filePreviews: {} });
        this.setState({ filePreviewsFinal: [] });
        this.props.setPropState('filePreviewsFinal', []);
        //this.setState({ filePreviewsFinal : [] });
        console.log('resetFile');
    
        return false;
    }


    GetImageAll(baseurl){
        var imgbase=baseurl;
        var gettype=this.base64MimeType(baseurl);

        let Image_return = [];

        if(gettype == 'image/png' || gettype == 'image/jpeg' || gettype == 'image/jpg' ){
            Image_return.push(<img src={baseurl} alt={'image preview'} style={{ width: 50, height:50, margin: 10 }} />);
        } else if(gettype == 'application/pdf'){
            Image_return.push(<img src={PdfImg} alt={'image preview'} style={{ width: 50, height:50, margin: 10 }} />);
        } else if(gettype == 'application/vnd.ms-excel'){
            Image_return.push(<img src={ExcelImg} alt={'image preview'} style={{ width: 50, height:50, margin: 10 }} />);
        } else if(gettype == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'){
            Image_return.push(<img src={ExcelImg} alt={'image preview'} style={{ width: 50, height:50, margin: 10 }} />);
        } else if(gettype == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' || gettype == 'application/msword'){
            Image_return.push(<img src={DocxImg} alt={'image preview'} style={{ width: 50, height:50, margin: 10 }} />);
        }

        return Image_return;
    }

    NewTabOpen = (baseurl) => e => {
        e.preventDefault();
        console.log(baseurl);
        
        const linkSource = baseurl;
        const downloadLink = document.createElement("a");
    

        downloadLink.href = linkSource;
        downloadLink.download = this.GetFileName(baseurl);
        downloadLink.click();
        return false;
    }

    GetImageAllNew(baseurl){
        var imgbase=baseurl;
        var gettype=this.base64MimeType(baseurl);

        let Image_return = [];
        let Image_return_Final = [];

        if(gettype == 'image/png' || gettype == 'image/jpeg' || gettype == 'image/jpg' ){
            var image_var = <img src={baseurl} alt={'image preview'} style={{ width: 50, height:50, margin: 10 }} />;
        } else if(gettype == 'application/pdf'){
            var image_var = <img src={PdfImg} alt={'image preview'} style={{ width: 50, height:50, margin: 10 }} />;
        } else if(gettype == 'application/vnd.ms-excel'){
            var image_var = <img src={ExcelImg} alt={'image preview'} style={{ width: 50, height:50, margin: 10 }} />;
        } else if(gettype == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'){
            var image_var = <img src={ExcelImg} alt={'image preview'} style={{ width: 50, height:50, margin: 10 }} />;
        } else if(gettype == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' || gettype == 'application/msword'){
            var image_var = <img src={DocxImg} alt={'image preview'} style={{ width: 50, height:50, margin: 10 }} />;
        } 

        Image_return_Final.push(
            <div className="col-6 col-sm-4 col-md-3 col-lg-4 col-xl-3">
                <div className="card card-file">
                  <div className="dropdown-file">
                    <a href="" className="dropdown-link" data-toggle="dropdown"><i className="fa fa-ellipsis-v" /></a>
                    <div className="dropdown-menu dropdown-menu-right">
                      <a href="#" onClick={this.NewTabOpen(baseurl)} className="dropdown-item">Download</a>
                      <a href="#" className="dropdown-item">Delete</a>
                    </div>
                  </div>
                  <div className="card-file-thumb">
                    {image_var}
                  </div>
                </div>
            </div>
        );



        return Image_return_Final;
    }

    


    render() {
    const { ResetFileMethod } = this.props;
    return (

       <div>
                <Upload
                    batch={false}
                    //autoUpload={false}
                    multiple={true}
                    restrictions={{
                        maxFileSize: 10000000,
                        allowedExtensions: [ '.jpeg','.jpg', '.png','.pdf','.xls','.xlsx','.csv','.docx','.doc']
                    }}
                    files={this.state.files}
                    onAdd={this.onAdd}
                    onRemove={this.onRemove}
                    onProgress={this.onProgress}
                    onStatusChange={this.onStatusChange}
                    withCredentials={false}
                    saveUrl={'https://demos.telerik.com/kendo-ui/service-v4/upload/save'}
                    removeUrl={'https://demos.telerik.com/kendo-ui/service-v4/upload/remove'}
                />
                 <div className={'example-config'} style={{ marginTop: 20, display: 'none' }}>
                    <ul className={'event-log'}>
                        {
                            this.state.events.map(event => <li>{event}</li>)
                        }
                    </ul>
                </div>

                {
                    this.state.files.length > 999 ?
                    <div>
                        <h4>Preview Files</h4>
                        <br/>
                        <div className="row row-sm">
                           {   
                                Object.keys(this.state.filePreviews).map((fileKey) => (
                                    this.GetImageAllNew(this.state.filePreviews[fileKey].FileData)
                                ))
                            }
                        </div> 
                    </div>: undefined
                }

                

                {this.props.ResetFileMethod == true ?
                    this.resetFile()
                    : null
                }
                <button type="hidden" class="removeDocumentFunc" onClick={this.removeFunc()} >Submit</button>
            </div>
    );
  }
}
export default FileUploadPreview;
