import DocxImg from '../../assets/img/doc/docx.png'
import ExcelImg from '../../assets/img/doc/excel.png'
import PdfImg from '../../assets/img/doc/pdf.png'

const FileUploadhelpers = {
    downloadFile: function(base64,gettype){
    	alert();
    	var today = new Date();
	    var y = today.getFullYear();
	    var m = today.getMonth() + 1;
	    var d = today.getDate();
	    var h = today.getHours();
	    var mi = today.getMinutes();
	    var s = today.getSeconds();
	    var ms = today.getMilliseconds();
	    var time = "PHSS000"+y  + m  + d  + h  + mi  + s + ms+this.randomFun();

	    var ext='';
	    if(gettype == 'image/png'){
	      ext=time+'.png';
	    }else if(gettype == 'image/jpeg'){
	        ext=time+'.jpg';
	    }else if(gettype == 'image/jpg'){
	        ext=time+'.jpg';
	    }else if(gettype == 'application/vnd.ms-excel'){
	        ext=time+'.csv';
	    }else if(gettype == 'application/pdf'){
	        ext=time+'.pdf';
	    }else if(gettype == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'){
	        ext=time+'.xlsx';
	    }else if(gettype == 'application/vnd.ms-excel'){
	        ext=time+'.xls';
	    }else if(gettype == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'){
	        ext=time+'.docx';
	    }
	    // console.log('downloadPDF');
	    // console.log(filenm);
	    // console.log(base64);
	    var createBase64 = "data:"+gettype+";base64,"+base64;
	    // console.log(createBase64);
	    const linkSource = createBase64;
	    const downloadLink = document.createElement("a");
	    const fileName = ext;

	    downloadLink.href = linkSource;
	    downloadLink.download = fileName;
	    downloadLink.click();
    },
    randomFun: function(){
    	var min = 100;
      	var max = 999;
      	return Math.floor(Math.random()*(max-min+1)+min);
    }
}

export default FileUploadhelpers;