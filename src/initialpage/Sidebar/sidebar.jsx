/**
 * App Header
 */
import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';

import Main from "../../Entryfile/Main";
import SystemHelpers from '../Helpers/SystemHelper';
import CryptoAES from 'crypto-js/aes';
import CryptoENC from 'crypto-js/enc-utf8';

class Sidebar extends Component {
  constructor(props) {
    super(props);

    this.state = {
        errormsg :  '',
        all_data :  [],
        //user_role :  {},
        
        /* role_func_call */
        role_func_call : false,
        get_profile_call : false,
        primarylocation: '',

        role_timesheet_can : {},
        role_scheduler_can : {},
        role_user_can : {},
        role_employees_can : {},
        role_locations_can :{},
        role_holidays_can :{},
        role_report_can :{},
        role_entitlement_menu_can : {},
        role_payroll_adjustment_menu_can : {},
        role_batch_report_menu_can : {},
        role_audit_report_menu_can : {}
        /* role_func_call */
           
    };
    this.handleChange = this.handleChange.bind(this);
    this.setPropState = this.setPropState.bind(this);
  }

  setPropState(key, value) {
      this.setState({ [key]: value });
  }

  

  // Loader hide show method
  hideLoader = () => {
    this.setState({ loading: false });
  }

  showLoader = () => {
    this.setState({ loading: true });
  }
  // Loader hide show method

  // toast hide show method
  ToastSuccess(msg){
      toast.success(msg, {
        position: "top-right",
        autoClose: process.env.API_TOAST_TIME,
        hideProgressBar: false,
        closeOnClick: false,
        pauseOnHover: false,
        draggable: false
      });
  }

  ToastError(msg){
      toast.error(msg, {
        position: "top-right",
        autoClose: process.env.API_TOAST_TIME,
        hideProgressBar: false,
        closeOnClick: false,
        pauseOnHover: false,
        draggable: false
      });
  }
  // toast hide show method
  push_func(){
    this.props.history.push("/");
    this.hideLoader();
  }

  SessionOut(){
    this.showLoader();
  
    localStorage.removeItem("token");
    localStorage.removeItem("contactId");
    localStorage.removeItem("eMailAddress");
    localStorage.removeItem("fullName");

    this.ToastError('Session Timeout');
    
    setTimeout( () => {this.push_func()}, 5000);
    
    return null;
  }

  // Input box Type method
  handleChange = input => e => {
    this.setState({ [input]: e.target.value });
  }
  componentDidMount() {
    
    $(document).ready(function() {
      var Sidemenu = function() {
        this.$menuItem = $('#sidebar-menu a');
      };
      
      function init() {

        var $this = Sidemenu;
        $('#sidebar-menu a').on('click', function(e) {
          
          if($(this).parent().hasClass('submenu')) {
            e.preventDefault();
          }
          if(!$(this).hasClass('subdrop')) {
            $('ul', $(this).parents('ul:first')).slideUp(350);
            $('a', $(this).parents('ul:first')).removeClass('subdrop');
            $(this).next('ul').slideDown(350);
            $(this).addClass('subdrop');
          } else if($(this).hasClass('subdrop')) {
            $(this).removeClass('subdrop');
            $(this).next('ul').slideUp(350);
          }
        });
        $('#sidebar-menu ul li.submenu a.active').parents('li:last').children('a:first').addClass('active').trigger('click');
      }
      
      // Sidebar Initiate
      init();
    });

    /* Role Management */
    // var pwd = localStorage.getItem("contactId")+"Phss@123";
    // var Role_session = localStorage.getItem('sessiontoken');

    // var _ciphertext = CryptoAES.decrypt(Role_session, pwd);
    // console.log('Role Store');
    // console.log(_ciphertext.toString(CryptoENC));
    // var JsonCreate = JSON.parse(_ciphertext.toString(CryptoENC));

    var getrole = SystemHelpers.GetRole();

    this.setState({ role_employees_can: getrole.employees_can });
    this.setState({ role_locations_can: getrole.locations_can });
    this.setState({ role_timesheet_can: getrole.timesheet_can });
    this.setState({ role_scheduler_can: getrole.scheduler_can });
    this.setState({ role_user_can: getrole.user_can });
    this.setState({ role_holidays_can: getrole.holidays_can });
    this.setState({ role_report_can: getrole.report_can });
    
    this.setState({ role_entitlement_menu_can: getrole.entitlement_menu_can });
    this.setState({ role_payroll_adjustment_menu_can: getrole.payroll_adjustment_menu_can });

    this.setState({ role_batch_report_menu_can: getrole.batch_report_menu_can });
    this.setState({ role_audit_report_menu_can: getrole.audit_report_menu_can });

    // console.log('Side bar Menu permission');

    // console.log(getrole);

    // console.log(getrole.employees_can);
    // console.log(getrole.locations_can);
    // console.log(getrole.timesheet_can);
    // console.log(getrole.user_can);
    // console.log(getrole.holidays_can);
    // console.log(getrole.report_can);
    // console.log(JsonCreate.employees_can);
    // console.log(getrole.entitlement_menu_can);
    // console.log(getrole.payroll_adjustment_menu_can);
    /* Role Management */
  }

  
  
   render() {
    
    const {  location } = this.props
    let pathname = location.pathname

    return (
        <div className="sidebar" id="sidebar">
          
        <div className="sidebar-inner slimscroll">
          <div id="sidebar-menu" className="sidebar-menu">
            <ul>
              <li className="menu-title"> 
                <span>Main</span>
              </li>
              <li className={pathname.includes('dashboard') ?"active" :""}> 
                <a href="/dashboard"><i className="la la-dashboard" /> <span>Dashboard</span></a>
              </li>

              <li className={pathname.includes('staff-profile') ?"active" :""}> 
                <a href="/staff-profile"><i className="la la-user" /> <span>Profile</span></a>
              </li>

              {this.state.role_employees_can.employees_can_view == true || this.state.role_employees_can.employees_can_viewall == true ?
                <li className={pathname.includes('employees') ?"active" :""}> 
                  <a href="/employees"><i className="la la-user-plus" /> <span>Employees</span></a>
                </li>
              :null }
              { (this.state.role_locations_can.locations_can_view == true || this.state.role_locations_can.locations_can_viewall == true) ?
                <li className={pathname.includes('location') ?"active" :""}> 
                  <a href="/location"><i className="la la-map-marker" /> <span>Location</span></a>
                </li>
              :null }
              {this.state.role_timesheet_can.timesheet_can_view == true  ?
              <li className="submenu">
                <a href="#"><i className="la la-edit" /> <span> Time Sheet</span> <span className="menu-arrow" /></a>
                <ul>
                    <li><a className={pathname.includes('my-timesheet') ?"active" :""} href="/my-timesheet">My Time Sheet</a></li>
                    {this.state.role_timesheet_can.timesheet_can_summary == true  ?
                      <li><a className={pathname.includes('attendance-summary') ?"active" :""} 
                        href="/attendance-summary">Time Sheet Summary</a></li>
                    :null }
                </ul>
              </li>
              :null }

              {this.state.role_holidays_can.holidays_can_view == true  || this.state.role_holidays_can.holidays_can_viewall == true?
                <li className={pathname.includes('holiday') ?"active" :""}> 
                    <a href="/holiday"><i className="la la-smile-o" /> <span>Holidays</span></a>
                </li>
              :null }
              
              
              {this.state.role_report_can.report_can_view == true || this.state.role_batch_report_menu_can.batch_report_menu_can_view == true || this.state.role_audit_report_menu_can.audit_report_menu_can_view == true ?
                <li className="submenu">
                  <a href="#"><i className="la la-file-text" /> <span> Reports</span> <span className="menu-arrow" /></a>
                  <ul>
                    {this.state.role_report_can.report_can_view == true ?
                      <li> 
                        <a  className={pathname.includes('ytdreports') ?"active" :""} href="/ytdreports">YTD Hours Reports</a>
                      </li>: null
                    }

                    {/*{this.state.role_batch_report_menu_can.batch_report_menu_can_view == true ?
                      <li> 
                        <a  className={pathname.includes('batchpayrollreports') ?"active" :""} href="/batchpayrollreports">Payroll Adjustments</a>
                      </li>: null
                    }*/}

                    {this.state.role_batch_report_menu_can.batch_report_menu_can_view == true ?
                      <li> 
                        <a  className={pathname.includes('exportbatchpayrollreports') ?"active" :""} href="/exportbatchpayrollreports">Batch Payroll Reports Export</a>
                      </li>: null
                    }

                    {this.state.role_audit_report_menu_can.audit_report_menu_can_view == true ?
                      <li> 
                        <a  className={pathname.includes('auditreports') ?"active" :""} href="/auditreports">AUDIT Reports</a>
                      </li>: null
                    }
                  </ul>
                </li>
                

              :null }

              
              {process.env.Availability_Tab == "true" && this.state.role_scheduler_can.scheduler_can_view == true  ?
              <li className="submenu">
                <a href="#"><i className="la la-calendar" /> <span>Schedule</span> <span className="menu-arrow" /></a>
                <ul>
                    <li><a className={pathname.includes('schedule') ?"active" :""} href="/schedule">Schedules</a></li>

                   
                </ul>
              </li>
              :null }
              
              
              {this.state.role_entitlement_menu_can.entitlement_menu_can_view == true  ?
                <li className={pathname.includes('entitlements') ?"active" :""}> 
                    <a href="/entitlements"><i className="la la-thumbs-up" /> <span>Entitlements</span></a>
                </li>
              :null }

              {this.state.role_payroll_adjustment_menu_can.payroll_adjustment_menu_can_view == true ?
                <li className="submenu">
                  <a href="#"><i className="la la-calculator" /> <span> Payroll Settings</span> <span className="menu-arrow" /></a>
                  <ul>
                    {/*<li> 
                      <a  className={pathname.includes('payrolladjustments') ?"active" :""} href="/payrolladjustments">Payroll Adjustments</a>
                    </li>*/}
                    <li> 
                      <a  className={pathname.includes('batchpayrollreports') ?"active" :""} href="/batchpayrollreports">Payroll Adjustments</a>
                    </li>
                  </ul>
                </li>
              :null }
              
            </ul>
          </div>
        </div>
      </div>
       
      );
   }
}

export default withRouter(Sidebar);
